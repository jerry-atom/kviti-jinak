import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import sitemap from '@astrojs/sitemap';
import tailwind from "@astrojs/tailwind";
import compressor from "astro-compressor";
import favicons from "astro-favicons";

// https://astro.build/config
export default defineConfig({
  compressHTML: import.meta.env.PROD,
  integrations: [
    react(),
    tailwind({
      applyBaseStyles: false,
    }),
    sitemap({
      filter: (page) => {
        const { pathname } = new URL(page);
        return !pathname.startsWith("/callback")
            && !pathname.startsWith("/ucet")
            && !pathname.startsWith("/resetovani-hesla");
      },
    }),
    compressor({ brotli: true, gzip: false }),
    favicons({
      name: "Kvítí jinak",
      short_name: "Kvítí jinak",
      theme_color: "#5d6052",
      themes: ["#5d6052", "#000"],
      manifest: {
        display: "standalone",
        display_override: ["fullscreen", "minimal-ui"],
        start_url: "/",
        id: import.meta.env.DEV ? "http://localhost:4321/" : "https://www.kviti-jinak.cz/",
        orientation: "landscape"
      },
      icons: {
        favicons: true,
        android: true,
        appleIcon: true,
        appleStartup: true,
        windows: true,
        yandex: true,
      },
    }),
  ],
  output: "static",
  site: "https://www.kviti-jinak.cz",
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          api: "modern-compiler",
        },
      },
    },
    server: {
      watch: {
        ignored: ["playwright-report/**/*", "test-results/**/*"],
      },
    },
  },
});
