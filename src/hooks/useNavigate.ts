import { useCallback } from "react";
import { path, type Route } from "../libs/routes";

export type NavigateOptions = {
  action?: "redirect" | "replace";
};
type Navigate = (route: Route & NavigateOptions) => void;

export function useNavigate(): Navigate {
  const navigate = useCallback<Navigate>((route) => {
    if (route.action === "replace") {
      window.history.pushState({}, "", path(route));
    } else {
      const url = new URL(window.location.href);
      url.pathname = path(route);
      window.location.href = url.toString();
    }
  }, []);

  return navigate;
}
