import { useEffect } from "react";
import { useAppStore } from "../stores/app";
import { selectIsLoggedIn } from "../stores/slices/auth";
import { useNavigate } from "./useNavigate";

export function useRequireAuth(): void {
  const isLoggedIn = useAppStore(selectIsLoggedIn);
  const navigate = useNavigate();

  useEffect(() => {
    if (!isLoggedIn) {
      navigate({ to: "/prihlaseni" });
    }
  }, [isLoggedIn, navigate]);
}
