import { useEffect } from "react";
import type { Role } from "../libs/types";
import { useAppStore } from "../stores/app";
import { selectHasUserRole } from "../stores/slices/profile";
import { useNavigate } from "./useNavigate";
import { useRequireAuth } from "./useRequireAuth";

export function useRestrictRole(role: Role): void {
  useRequireAuth();
  const navigate = useNavigate();
  const hasRole = useAppStore((state) => selectHasUserRole(state, role));

  useEffect(() => {
    if (!hasRole) {
      navigate({ to: "/ucet" });
    }
  }, [hasRole, navigate]);
}
