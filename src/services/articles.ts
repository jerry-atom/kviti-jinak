import { z } from "zod";
import { type Picture, PictureIdSchema, UserId } from "../libs/types";

const basePictureSchema = z.object({
  id: PictureIdSchema,
  metadata: z.object({
    height: z.number(),
    mime: z.string(),
    width: z.number(),
  }),
  name: z.string(),
  type: z.enum(["jpg", "png", "webp", "avif"]),
});
const PictureSchema = z.discriminatedUnion("source", [
  basePictureSchema.extend({
    source: z.literal("cloudinary"),
    props: z.object({
      path: z.string().url(),
    }),
  }),
  basePictureSchema.extend({
    source: z.literal("gallery"),
    props: z.object({}),
  }),
  basePictureSchema.extend({
    source: z.literal("web"),
    props: z.object({
      url: z.string().url(),
    }),
  }),
]);

export const __assertPictureEqual: Picture = {} as z.infer<
  typeof PictureSchema
>;

export const ArticleIdSchema = z.string().uuid().brand("ArticleId");
export type ArticleId = z.infer<typeof ArticleIdSchema>;

export type Article = {
  author: UserId;
  content: string;
  created: Date;
  id: ArticleId;
  modified: Date | null;
  perex: string;
  picture: Picture | null;
  published: Date | null;
  seo: {
    author: string;
    genre: string;
    keywords: string;
  };
  slug: string;
  title: string;
};

const seoSchema = z.object({
  author: z.string(),
  genre: z.string(),
  keywords: z.string(),
});
export const ArticleSchema = z.object({
  author: UserId,
  content: z.string(),
  created: z.coerce.date(),
  id: ArticleIdSchema,
  modified: z.coerce.date().nullable(),
  perex: z.string(),
  picture: PictureSchema.nullable(),
  published: z.coerce.date().nullable(),
  seo: seoSchema,
  slug: z.string(),
  title: z.string(),
});

export const __assertArticleEqual: Article = {} as z.infer<
  typeof ArticleSchema
>;
