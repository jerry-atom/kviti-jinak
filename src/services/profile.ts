import { isAxiosError } from "axios";
import { api } from "../libs/api";
import type { User } from "../libs/types";

type ProfileResult =
  | { ok: true; user: User }
  | { ok: false; code: "not_found" }
  | { ok: false; code: "unauthorized" }
  | { ok: false; code: "unknown" };

export async function fetchProfile(): Promise<ProfileResult> {
  try {
    const response = await api.get("/profile");
    return { ok: true, ...response.data };
  } catch (error) {
    if (isAxiosError(error)) {
      switch (error.status) {
        case 401:
          return { ok: false, code: "unauthorized" };
        case 404:
          return { ok: false, code: "not_found" };
      }
    }
    console.error(error);
    return { ok: false, code: "unknown" };
  }
}
