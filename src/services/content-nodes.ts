import { z } from "zod";

export const ContentNodeSchema = z.discriminatedUnion("type", [
  z.object({
    type: z.literal("text"),
    id: z.string(),
    value: z.string(),
  }),
  z.object({
    type: z.literal("link"),
    id: z.string(),
    value: z.any(),
  }),
  z.object({
    type: z.literal("picture"),
    id: z.string(),
    value: z.any(),
  }),
]);

export type ContentNode = z.infer<typeof ContentNodeSchema>;
