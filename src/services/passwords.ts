import { isAxiosError } from "axios";
import { api } from "../libs/api";

export type ChangePasswordParams = {
  newPassword: string;
  oldPassword: string;
};
type ChangePasswordResult =
  | { ok: true; data: { messageId: string } }
  | { ok: false; code: "password_not_found" }
  | { ok: false; code: "password_too_weak" }
  | { ok: false; code: "sending_email_failed" }
  | { ok: false; code: "wrong_password" }
  | { ok: false; code: "unknown_error" };

export async function changePassword(
  params: ChangePasswordParams,
): Promise<ChangePasswordResult> {
  try {
    const response = await api.post("/passwords/change", params);
    return { ok: true, data: response.data };
  } catch (error) {
    if (isAxiosError(error) && error.response) {
      return { ok: false, code: error.response.data.code };
    }
    console.error(error);
    return { ok: false, code: "unknown_error" };
  }
}

export type ForgettenPasswordParams = { email: string };
type ForgettenPasswordResult = { ok: true } | { ok: false };

export async function forgettenPassword(
  params: ForgettenPasswordParams,
): Promise<ForgettenPasswordResult> {
  const response = await api.post("/passwords/forgetten", params);
  if (response.status === 204) {
    return { ok: true };
  }
  return { ok: false };
}

type ResetPasswordResult =
  | { ok: true; data: { messageId: string } }
  | { ok: false; code: "email_failed" }
  | { ok: false; code: "token_invalid" }
  | { ok: false; code: "token_expired" }
  | { ok: false; code: "token_resolved" };

export async function resetPassword(params: {
  password: string;
  token: string;
}): Promise<ResetPasswordResult> {
  const response = await api.post("/passwords/reset", params);
  if (response.status === 200) {
    return { ok: true, data: response.data };
  }
  return { ok: false, code: response.data.code };
}
