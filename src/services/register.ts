import { api } from "../libs/api";

export type RegisterResult = { messageId: string };

export type RegisterData = {
  email: string;
  phone: string;
  name: string;
  password: string;
  termsOfService: boolean;
};

export async function register(data: RegisterData): Promise<RegisterResult> {
  const response = await api.post("/account/register", data);
  return response.data;
}
