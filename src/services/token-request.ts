import { z } from "zod";
import { api } from "../libs/api";

export const TokenRequestIdSchema = z.string().uuid().brand("TokenRequestId");
export type TokenRequestId = z.infer<typeof TokenRequestIdSchema>;

export type FetchTokenRequestResult =
  | { ok: true }
  | {
      ok: false;
      code:
        | "failed"
        | "token_expired"
        | "token_invalid"
        | "token_not_found"
        | "token_resolved";
    };

export async function fetchTokenRequest(
  token: TokenRequestId,
): Promise<FetchTokenRequestResult> {
  try {
    const response = await api.get(`/token-requests/${token}`);
    if (response.status === 200) {
      return { ok: true };
    }
    if (response.status === 400) {
      return { ok: false, code: "token_invalid" };
    }
    if (response.status === 404) {
      return { ok: false, code: "token_not_found" };
    }
    if (response.status === 409) {
      return { ok: false, code: "token_expired" };
    }
    if (response.status === 410) {
      return { ok: false, code: "token_resolved" };
    }
  } catch (error) {
    console.error(error);
  }
  return { ok: false, code: "failed" };
}

export function isTokenValid(value: unknown): value is TokenRequestId {
  return TokenRequestIdSchema.safeParse(value).success;
}
