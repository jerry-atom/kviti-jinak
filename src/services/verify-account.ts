import { isAxiosError } from "axios";
import { api } from "../libs/api";

export type VerifyAccountResult =
  | { ok: true }
  | { ok: false; code: "token_invalid" }
  | { ok: false; code: "token_not_found" }
  | { ok: false; code: "token_expired" }
  | { ok: false; code: "token_resolved" };

export async function verifyAccount(
  token: string,
): Promise<VerifyAccountResult> {
  try {
    await api.post("/account/verify", { token });
    return { ok: true };
  } catch (error) {
    if (!isAxiosError(error)) {
      throw new Error("Verifying account failed", { cause: error });
    }
    switch (error.response?.status) {
      case 400:
        return { ok: false, code: "token_invalid" };
      case 404:
        return { ok: false, code: "token_not_found" };
      case 410:
        return { ok: false, code: "token_resolved" };
    }
    return { ok: false, code: "token_invalid" };
  }
}
