import { api } from "../libs/api";
import type { Role } from "../libs/types";

export type LoginParams = {
  email: string;
  password: string;
};
type LoginResult =
  | { ok: true; token: string; role: Role }
  | { ok: false; code: "password_not_found" }
  | { ok: false; code: "unknown_error" }
  | { ok: false; code: "wrong_credentials" };

export async function login(params: LoginParams): Promise<LoginResult> {
  const url = new URL(import.meta.env.PUBLIC_API_URL);
  url.pathname = "/account/login";
  const response = await fetch(url.toString(), {
    body: JSON.stringify(params),
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    mode: "cors",
  });
  if (response.status === 200) {
    return { ok: true, ...(await response.json()) };
  }
  if (response.status === 400) {
    return { ok: false, ...(await response.json()) };
  }
  return { ok: false, code: "unknown_error" };
}

export async function logout(): Promise<{ ok: boolean }> {
  const response = await api.post("/account/logout", { yes: true });
  return { ok: response.status < 400 };
}
