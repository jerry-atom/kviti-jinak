import { z } from "zod";
import { formatTime } from "../libs/i18n";

export const WeekdaySchema = z.enum([
  "mon",
  "tue",
  "wed",
  "thu",
  "fri",
  "sat",
  "sun",
]);
export type Weekday = z.infer<typeof WeekdaySchema>;

export const BusinessHoursSchema = z.object({
  id: z.string(),
  description: z.string().optional(),
  weekdays: z.array(WeekdaySchema),
  validFrom: z.string(),
  validTo: z.string().optional(),
  opens: z.string(),
  closes: z.string(),
});
export type BusinessHours = z.infer<typeof BusinessHoursSchema>;

export function isExceptional(businessHours: BusinessHours): boolean {
  return businessHours.weekdays.length === 0;
}

export function isOrdinary(businessHours: BusinessHours): boolean {
  return !isExceptional(businessHours);
}

export function isClosed(businessHours: BusinessHours): boolean {
  return businessHours.opens === businessHours.closes;
}

export function buildOpeningHoursSpecification(businessHours: BusinessHours[]) {
  const weekdayNames: Record<Weekday, string> = {
    mon: "https://schema.org/Monday",
    tue: "https://schema.org/Tuesday",
    wed: "https://schema.org/Wednesday",
    thu: "https://schema.org/Thursday",
    fri: "https://schema.org/Friday",
    sat: "https://schema.org/Saturday",
    sun: "https://schema.org/Sunday",
  };
  const currentDate = new Date().toISOString().substring(0, 11);
  return businessHours.map((value) => ({
    "@type": "OpeningHoursSpecification",
    closes: formatTime(new Date(`${currentDate}${value.closes}:00`)),
    dayOfWeek:
      value.weekdays.length === 0
        ? undefined
        : value.weekdays.map((weekday) => weekdayNames[weekday]),
    description: value.description,
    opens: formatTime(new Date(`${currentDate}${value.opens}:00`)),
    validFrom: value.validFrom,
    validThrough: value.validTo,
  }));
}
