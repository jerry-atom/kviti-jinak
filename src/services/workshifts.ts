import { isAxiosError } from "axios";
import useSWR, { mutate } from "swr";
import { z } from "zod";
import { api, fetcher } from "../libs/api";
import type { User, UserId } from "../libs/types";

export const AttendanceRecordIdSchema = z
  .string()
  .uuid()
  .brand("AttendanceRecordId");
export type AttendanceRecordId = z.infer<typeof AttendanceRecordIdSchema>;

export type AttendanceRecordType =
  | "work_start"
  | "work_end"
  | "pause_start"
  | "pause_end";
export type AttendanceRecord = {
  id: AttendanceRecordId;
  time: string;
  type: AttendanceRecordType;
};

export const WorkshiftIdSchema = z.string().uuid().brand("WorkshiftId");
export type WorkshiftId = z.infer<typeof WorkshiftIdSchema>;

export type Workshift = {
  end: string;
  id: WorkshiftId;
  isLocked: boolean;
  name: string;
  note?: string;
  start: string;
};

export const WorkshiftAssignmentIdSchema = z
  .string()
  .uuid()
  .brand("WorkshiftAssignmentId");
export type WorkshiftAssignmentId = z.infer<typeof WorkshiftAssignmentIdSchema>;

export type WorkshiftAssignment = {
  allowOvertime?: boolean;
  id: WorkshiftAssignmentId;
  employee: UserId;
  workshift: WorkshiftId;
  maxAllowedOvertime?: number;
  overtimeApproved?: number;
  overtimeTotal?: number;
  pauseTime?: number;
  workTime?: number;
};

export type CurrentWorkshiftState = {
  attendanceRecords: AttendanceRecord[];
  previousType: AttendanceRecordType;
  nextTypes: AttendanceRecordType[];
  workshift: Omit<Workshift, "attendanceRecords">;
  pauseLength: number;
  workLength: number;
};

export const useLatestAttendanceRecords = () =>
  useSWR<AttendanceRecord[]>("/attendance/latest", fetcher);

export const useCurrentWorkshift = () =>
  useSWR<{ ok: true; data: Workshift } | { ok: false; error: "not_found" }>(
    "/workshifts/current",
    fetcher,
  );

export const useTodaysWorkshifts = () =>
  useSWR<Workshift[]>("/workshifts/today", fetcher);

export const useWorkshiftAssignments = (workshiftId: WorkshiftId) =>
  useSWR<WorkshiftAssignment[]>(
    `/workshifts/${workshiftId}/assignments`,
    fetcher,
  );

export const useUser = (userId: UserId) =>
  useSWR<User>(`/users/${userId}`, fetcher);

export const useWorkshifts = (month: Date) =>
  useSWR<Workshift[]>(
    `/workshifts/${month.getFullYear()}/${month.getMonth() + 1}`,
    fetcher,
  );

export async function appendAttendaceRecord(
  attendanceRecordType: AttendanceRecordType,
): Promise<
  | { ok: true; data: AttendanceRecord[] }
  | { ok: false; error: "conflicting_attendance" }
> {
  try {
    const response = await api.post("/attendance", { attendanceRecordType });
    await mutate("/attendance/latest");
    return { ok: true, data: response.data };
  } catch (error) {
    if (isAxiosError(error)) {
      switch (error.status) {
        case 409:
          return { ok: false, error: "conflicting_attendance" };
      }
    }
    throw new Error("Appending attendace record", { cause: error });
  }
}

export type AttendanceReport = {
  contractedHours: number;
  dateStart: string;
  dateEnd: string;
  totalOvertime: number;
  totalOvertimeApproved: number;
  totalPauseTime: number;
  totalWorkTime: number;
  unpaidLeave: number;
  vacationClaim: number;
  vacationUsed: number;
};

export const useAttendanceReportsByYear = (year: number) =>
  useSWR<AttendanceReport[]>(`/attendance/report/${year}`);
