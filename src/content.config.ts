import { defineCollection } from "astro:content";
import { z } from "zod";
import { ArticleSchema } from "./services/articles";
import { BusinessHoursSchema } from "./services/business-hours";
import { ContentNodeSchema } from "./services/content-nodes";

const articles = defineCollection({
  loader: async () => {
    const url = new URL(import.meta.env.PUBLIC_API_URL);
    url.pathname = "/articles";
    try {
      const response = await fetch(url, {
        method: "GET",
        mode: "cors",
        headers: {
          Accepts: "application/json",
          Authorization: `Bearer ${import.meta.env.SYSTEM_TOKEN}`,
        },
      });
      return await response.json();
    } catch (cause) {
      throw new Error("Cannot load all published articles", { cause });
    }
  },
  schema: ArticleSchema,
});

const RawBusinessHours = z.array(
  BusinessHoursSchema.extend({ id: z.number() }),
);

const businessHours = defineCollection({
  loader: async () => {
    const url = new URL(import.meta.env.PUBLIC_API_URL);
    url.pathname = "/business-hours";
    try {
      const response = await fetch(url, {
        method: "GET",
        mode: "cors",
        headers: {
          Accepts: "application/json",
          Authorization: `Bearer ${import.meta.env.SYSTEM_TOKEN}`,
        },
      });
      const rawData = await response.json();
      const data = RawBusinessHours.parse(rawData);
      return data.map((x) => ({ ...x, id: String(x.id) }));
    } catch (cause) {
      throw new Error("Cannot load business hours", { cause });
    }
  },
  schema: BusinessHoursSchema,
});

const contentNodes = defineCollection({
  type: "content_layer",
  loader: async () => {
    const url = new URL(import.meta.env.PUBLIC_API_URL);
    url.pathname = "/content-nodes";
    try {
      const response = await fetch(url, {
        method: "GET",
        mode: "cors",
        headers: {
          Accepts: "application/json",
          Authorization: `Bearer ${import.meta.env.SYSTEM_TOKEN}`,
        },
      });
      const data = await response.json();
      return data;
    } catch (cause) {
      throw new Error("Cannot load content nodes", { cause });
    }
  },
  schema: ContentNodeSchema,
});

export const collections = { articles, businessHours, contentNodes };
