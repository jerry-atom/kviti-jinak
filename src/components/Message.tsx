import classNames from "classnames";
import type { PropsWithChildren } from "react";
import {
  TbCircleCheck,
  TbExclamationCircle,
  TbInfoCircle,
} from "react-icons/tb";

type Props = PropsWithChildren<{
  className?: string;
  color: "danger" | "success" | "info" | "primary" | "warning";
  title?: string;
}>;

export function Message({ children, className, color, title }: Props) {
  return (
    <div
      className={classNames(
        "flex flex-row gap-3 border-2 p-3 rounded-md",
        {
          "border-danger-dark bg-danger-medium text-danger-dark":
            color === "danger",
          "border-info-dark bg-info-medium text-info-dark": color === "info",
          "border-primary-dark bg-primary-medium text-primary-dark":
            color === "primary",
          "border-success-dark bg-success-medium text-success-dark":
            color === "success",
          "border-warning-dark bg-warning-medium text-warning-dark":
            color === "warning",
        },
        className,
      )}
      role="alert"
    >
      <div className="block flex-grow-0 py-1">
        {color === "danger" && <TbExclamationCircle size={24} />}
        {color === "info" && <TbInfoCircle size={24} />}
        {color === "primary" && <TbInfoCircle size={24} />}
        {color === "success" && <TbCircleCheck size={24} />}
        {color === "warning" && <TbInfoCircle size={24} />}
      </div>
      <div className="flex-grow">
        {title && <h4 className="text-lg">{title}</h4>}
        {children}
      </div>
    </div>
  );
}
