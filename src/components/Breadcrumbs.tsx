import { Fragment } from "react";
import { TbChevronRight } from "react-icons/tb";
import { path, type Route } from "../libs/routes";

export type Breadcrumb = Route & {
  label: string;
};

export type Props = {
  breadcrumbs: Breadcrumb[];
};

export function Breadcrumbs({ breadcrumbs }: Props) {
  return (
    <div className="container my-3">
      <nav
        className="flex flex-row gap-1 justify-start items-center text-primary"
        aria-label="Drobečková navigace"
      >
        <a className="hover:underline" href={path({ to: "/" })}>
          Domů
        </a>
        {breadcrumbs.map((x, i, a) => (
          <Fragment key={path(x)}>
            <TbChevronRight className="inline" />
            <a
              className="hover:underline"
              href={path(x)}
              {...(i === a.length - 1 ? { "aria-current": "page" } : {})}
            >
              {x.label}
            </a>
          </Fragment>
        ))}
      </nav>
    </div>
  );
}
