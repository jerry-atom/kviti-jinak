import { FacebookFLogo } from "./FacebookFLogo";
import { ButtonLink } from "./form/ButtonLink";

type Props = {
  label: string;
  redirect: string;
};

export function FacebookAuthButton({ label, redirect }: Props) {
  const url = new URL(import.meta.env.PUBLIC_API_URL);
  url.pathname = "/oauth2/facebook";
  url.searchParams.set("redirect_uri", redirect);

  return (
    <ButtonLink
      color="secondary"
      className="w-full flex flex-row justify-center items-center gap-2"
      href={url.toString()}
    >
      <FacebookFLogo />
      {label}
    </ButtonLink>
  );
}
