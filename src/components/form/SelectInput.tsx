import classNames from "classnames";
import type { ReactElement } from "react";
import { TbSelector } from "react-icons/tb";
import { InputLabel } from "./InputLabel";
import type { CommonInputProps } from "./types";

export type Props = CommonInputProps<string> & {
  className?: string | undefined;
  data: Array<{ label: string; value: string }>;
  description?: string | ReactElement;
  placeholder?: string | boolean;
};

export function SelectInput({
  className,
  description,
  error,
  showFeedback,
  ...props
}: Props) {
  return (
    <div className={className}>
      <InputLabel htmlFor={props.id} required={props.required}>
        {props.label}
      </InputLabel>
      <div className="relative inline-block w-full max-w-xs">
        <select
          autoComplete={props.autoComplete}
          className={classNames(
            "block appearance-none bg-white border-2 outline-none w-full focus-visible:ring-ring focus-visible:outline-none focus-visible:ring-2 rounded-md px-4 py-2 pr-8 leading-relaxed",
            {
              "border-primary-medium": !showFeedback,
              "border-success": showFeedback && error == null,
              "border-danger": showFeedback && error != null,
            },
          )}
          disabled={props.disabled}
          id={props.id}
          name={props.name}
          onChange={(event) => {
            props.onChange(event.target.value);
          }}
          required={props.required}
          value={props.value}
        >
          {typeof props.placeholder === "string" ? (
            <option value="">{props.placeholder}</option>
          ) : props.placeholder === true ? (
            <option value="">vyberte&hellip;</option>
          ) : null}
          {props.data.map(({ label, value }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-secondary">
          <TbSelector />
        </div>
      </div>
      {error && (
        <div
          className="mx-2 my-1 text-danger"
          id={`${props.id}-feedback`}
          role="alert"
        >
          {error}
        </div>
      )}
      {description && (
        <div id={`${props.id}-help`} className="mx-2 my-1">
          {description}
        </div>
      )}
    </div>
  );
}
