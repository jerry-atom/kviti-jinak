import classNames from "classnames";
import type { ReactElement } from "react";
import { Feedback } from "./Feedback";
import { InputLabel } from "./InputLabel";
import type { CommonInputProps } from "./types";

type Option = {
  label: string | ReactElement;
  value: string;
};

export type Props = Omit<CommonInputProps<string>, "label"> & {
  className?: string;
  data: Option[];
};

export function RadioInput({ className, data, error, ...props }: Props) {
  return (
    <div className="flex flex-col gap-3">
      {data.map(({ label, value }) => (
        <div className="flex flex-row gap-3" key={value}>
          <input
            aria-describedby={classNames({
              [`${props.id}-required`]: props.required,
              [`${props.id}-feedback`]: error != null,
            })}
            checked={value === props.value}
            className={classNames([
              "flex-grow-0 accent-primary w-6 h-6 focus-within:ring-ring focus-within:outline-none focus-within:ring-2",
              {
                "border-primary": !props.showFeedback,
                "border-success": props.showFeedback && error == null,
                "border-danger": props.showFeedback && error != null,
              },
              className,
            ])}
            disabled={props.disabled}
            id={`${props.id}-${value}`}
            onChange={(event) => {
              props.onChange(event.target.value);
            }}
            name={props.name}
            required={props.required}
            type="radio"
            value={value}
          />
          <InputLabel
            checkbox
            htmlFor={`${props.id}-${value}`}
            required={props.required}
          >
            {label}
          </InputLabel>
        </div>
      ))}
      {error && <Feedback id={`${props.id}-feedback`}>{error}</Feedback>}
    </div>
  );
}
