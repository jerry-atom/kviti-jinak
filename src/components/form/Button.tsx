import classNames from "classnames";
import type { DOMAttributes, PropsWithChildren } from "react";

type Props = PropsWithChildren<
  {
    color?:
      | "danger"
      | "danger-outline"
      | "primary"
      | "primary-outline"
      | "secondary"
      | "secondary-outline"
      | "success"
      | "success-outline";
    isLoading?: boolean;
  } & Pick<DOMAttributes<HTMLButtonElement>, "onClick"> &
    Partial<Pick<HTMLButtonElement, "className" | "disabled" | "type">>
>;

export function Button({
  children,
  className,
  color = "primary",
  isLoading = false,
  type = "button",
  ...props
}: Props) {
  return (
    <button
      className={classNames(
        "rounded-md focus-visible:ring-ring focus-visible:outline-none focus-visible:ring-2 px-3 py-2 leading-relaxed",
        isLoading
          ? "bg-zinc-300 text-zinc-500 border-zinc-500"
          : {
              "bg-danger hover:bg-danger-medium text-white": color === "danger",
              "border-2 border-danger hover:bg-danger-medium text-danger":
                color === "danger-outline",
              "bg-primary hover:bg-primary-medium text-white":
                color === "primary",
              "border-2 border-primary hover:bg-primary-light text-primary":
                color === "primary-outline",
              "bg-secondary hover:bg-secondary-medium text-black":
                color === "secondary",
              "border-2 border-secondary hover:bg-secondary-medium text-black":
                color === "secondary-outline",
              "bg-success hover:bg-success-medium text-white":
                color === "success",
              "border-2 border-success hover:bg-success-medium text-success":
                color === "success-outline",
            },
        className,
      )}
      type={type}
      disabled={isLoading}
      {...props}
    >
      {children}
    </button>
  );
}
