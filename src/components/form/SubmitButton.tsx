import type { PropsWithChildren } from "react";
import { Button } from "./Button";

type Props = PropsWithChildren<{
  disabled?: boolean;
}>;

export function SubmitButton(props: Props) {
  return (
    <Button
      className="w-full text-center"
      disabled={props.disabled}
      type="submit"
    >
      {props.children}
    </Button>
  );
}
