import type { PropsWithChildren } from "react";

export type Props = PropsWithChildren<{
  checkbox?: boolean;
  required?: boolean;
  htmlFor: string;
  type?: "checkbox";
}>;

export function InputLabel(props: Props) {
  return (
    <div className={props.checkbox ? "" : "mb-2"}>
      {props.required && (
        <>
          <span className="sr-only" id={`${props.htmlFor}-required`}>
            Povinná položka
          </span>
          <span className="text-danger me-2" title="Povinná položka">
            {"!"}
          </span>
        </>
      )}
      <label htmlFor={props.htmlFor} id={`${props.htmlFor}-label`}>
        {props.children}
      </label>
    </div>
  );
}
