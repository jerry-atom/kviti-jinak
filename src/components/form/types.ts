import type { ReactElement } from "react";

export type CommonInputProps<T> = {
  autoComplete?: string;
  autoFocus?: boolean;
  className?: string | undefined;
  disabled?: boolean;
  error?: string;
  id: string;
  label: string | ReactElement;
  name?: string;
  onChange: (value: T) => void;
  required?: boolean;
  showFeedback?: boolean;
  value?: T;
};
