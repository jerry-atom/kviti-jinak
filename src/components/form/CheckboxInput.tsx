import classNames from "classnames";
import { Feedback } from "./Feedback";
import { InputLabel } from "./InputLabel";
import type { CommonInputProps } from "./types";

export type Props = Omit<CommonInputProps<boolean>, "value" | "type"> & {
  checked?: boolean;
  value?: string;
};

export function CheckboxInput({ className, error, onChange, ...props }: Props) {
  return (
    <div className="flex flex-row gap-3">
      <input
        {...props}
        aria-describedby={classNames({
          [`${props.id}-required`]: props.required,
          [`${props.id}-feedback`]: error != null,
        })}
        className={classNames([
          "flex-grow-0 accent-primary w-6 h-6 focus-within:ring-ring focus-within:outline-none focus-within:ring-2",
          {
            "border-primary": !props.showFeedback,
            "border-success": props.showFeedback && error == null,
            "border-danger": props.showFeedback && error != null,
          },
          className,
        ])}
        onChange={(event) => onChange(event.target.checked)}
        type="checkbox"
      />
      <div className="flex flex-col">
        <InputLabel checkbox htmlFor={props.id} required={props.required}>
          {props.label}
        </InputLabel>
        {error && <Feedback id={`${props.id}-feedback`}>{error}</Feedback>}
      </div>
    </div>
  );
}
