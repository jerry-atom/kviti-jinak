import classNames from "classnames";
import type { AnchorHTMLAttributes, PropsWithChildren } from "react";

type Props = PropsWithChildren<
  {
    color?:
      | "danger"
      | "danger-outline"
      | "primary"
      | "primary-outline"
      | "secondary"
      | "secondary-outline"
      | "success"
      | "success-outline"
      | "warning"
      | "warning-outline";
  } & Pick<AnchorHTMLAttributes<HTMLAnchorElement>, "href"> &
    Partial<Pick<AnchorHTMLAttributes<HTMLAnchorElement>, "className">>
>;

export function ButtonLink({
  children,
  className,
  color = "primary",
  ...props
}: Props) {
  return (
    <a
      className={classNames(
        "rounded-md focus-within:ring-ring focus-within:outline-none focus-within:ring-2 px-3 py-2",
        {
          "bg-danger hover:bg-danger-medium text-white": color === "danger",
          "border-2 border-danger hover:bg-danger-medium text-danger":
            color === "danger-outline",
          "bg-primary hover:bg-primary-medium text-white": color === "primary",
          "border-2 border-primary hover:bg-primary-medium text-primary":
            color === "primary-outline",
          "bg-secondary hover:bg-secondary-medium text-white":
            color === "secondary",
          "border-2 border-secondary hover:bg-secondary-medium text-black":
            color === "secondary-outline",
          "bg-success hover:bg-success-medium text-white": color === "success",
          "border-2 border-success hover:bg-success-medium text-success":
            color === "success-outline",
          "bg-warning hover:bg-warning-medium text-white": color === "warning",
          "border-2 border-warning hover:bg-warning-medium text-warning":
            color === "warning-outline",
        },
        className,
      )}
      {...props}
    >
      {children}
    </a>
  );
}
