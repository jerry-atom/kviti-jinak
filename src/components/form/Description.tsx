import type { PropsWithChildren } from "react";

type Props = PropsWithChildren<{ id: string }>;

export function Description({ children, id }: Props) {
  return (
    <div id={id} className="mx-2 my-1">
      {children}
    </div>
  );
}
