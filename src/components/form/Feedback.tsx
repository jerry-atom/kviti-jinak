import type { PropsWithChildren } from "react";

type Props = PropsWithChildren<{ id: string }>;

export function Feedback({ children, id }: Props) {
  return (
    <div className="mx-2 my-1 text-danger" id={id} role="alert">
      {children}
    </div>
  );
}
