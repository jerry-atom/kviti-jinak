import classNames from "classnames";
import { type ReactElement, useState } from "react";
import { TbEye, TbEyeClosed } from "react-icons/tb";
import { Description } from "./Description";
import { Feedback } from "./Feedback";
import { InputLabel } from "./InputLabel";
import type { CommonInputProps } from "./types";

export type Props = CommonInputProps<string> & {
  description?: string | ReactElement;
  placeholder?: string;
};

export function PasswordInput({
  className,
  description,
  error,
  ...props
}: Props) {
  const [showPassword, setShowPassword] = useState(false);

  return (
    <div className={className}>
      <InputLabel htmlFor={props.id} required={props.required}>
        {props.label}
      </InputLabel>
      <div
        className={classNames([
          "flex flex-row rounded-md overflow-hidden border-2 w-full focus-within:ring-ring focus-within:outline-none focus-within:ring-2",
          {
            "border-primary-medium": !props.showFeedback,
            "border-success": props.showFeedback && error == null,
            "border-danger": props.showFeedback && error != null,
          },
        ])}
      >
        <input
          aria-describedby={classNames({
            [`${props.id}-visibility`]: true,
            [`${props.id}-required`]: props.required,
            [`${props.id}-help`]: description != null,
            [`${props.id}-feedback`]: error != null,
          })}
          autoComplete={props.autoComplete}
          className={classNames({
            "block form-input border-none outline-none flex-grow m-0 px-3 py-2": true,
          })}
          disabled={props.disabled}
          id={props.id}
          name={props.name}
          onChange={(event) => {
            props.onChange(event.target.value);
          }}
          placeholder={props.placeholder}
          required={props.required}
          type={showPassword ? "text" : "password"}
          value={props.value}
        />
        <button
          aria-controls={props.id}
          aria-label={showPassword ? "Skrýt" : "Zobrazit"}
          aria-pressed={showPassword}
          className="block flex-grow-0 border-0 outline-none px-3 text-secondary focus:bg-primary-light"
          id={`${props.id}-visibility`}
          onClick={() => setShowPassword((x) => !x)}
          title={showPassword ? "Skrýt" : "Zobrazit"}
          type="button"
        >
          {showPassword ? (
            <>
              <span className="sr-only">Skrýt</span>
              <TbEyeClosed size={24} />
            </>
          ) : (
            <>
              <span className="sr-only">Zobrazit</span>
              <TbEye size={24} />
            </>
          )}
        </button>
      </div>
      {error && <Feedback id={`${props.id}-feedback`}>{error}</Feedback>}
      {description && (
        <Description id={`${props.id}-help`}>{description}</Description>
      )}
    </div>
  );
}
