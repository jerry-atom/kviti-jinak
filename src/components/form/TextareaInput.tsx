import classNames from "classnames";
import type { ReactElement } from "react";
import { Description } from "./Description";
import { Feedback } from "./Feedback";
import { InputLabel } from "./InputLabel";
import type { CommonInputProps } from "./types";

export type Props = CommonInputProps<string> & {
  cols?: number;
  description?: string | ReactElement;
  placeholder?: string;
  rows?: number;
};

export function TextareaInput({
  className,
  error,
  description,
  showFeedback,
  ...props
}: Props) {
  return (
    <div className={className}>
      <InputLabel htmlFor={props.id} required={props.required}>
        {props.label}
      </InputLabel>
      <textarea
        autoComplete={props.autoComplete}
        aria-describedby={classNames({
          [`${props.id}-required`]: props.required,
          [`${props.id}-help`]: description != null,
          [`${props.id}-feedback`]: error != null,
        })}
        className={classNames(
          "border-2 outline-none w-full focus-visible:ring-ring focus-visible:outline-none focus-visible:ring-2 rounded-md px-3 py-2 leading-relaxed",
          {
            "border-primary-medium": !showFeedback,
            "border-success": showFeedback && error == null,
            "border-danger": showFeedback && error != null,
          },
        )}
        cols={props.cols}
        disabled={props.disabled}
        id={props.id}
        name={props.name}
        onChange={(event) => {
          props.onChange(event.target.value);
        }}
        placeholder={props.placeholder}
        required={props.required}
        rows={props.rows}
        value={props.value}
      />
      {error && <Feedback id={`${props.id}-feedback`}>{error}</Feedback>}
      {description && (
        <Description id={`${props.id}-help`}>{description}</Description>
      )}
    </div>
  );
}
