import { TbAlarm, TbCalendar, TbLogin, TbLogout, TbUser } from "react-icons/tb";
import { path } from "../libs/routes";
import { useAppStore } from "../stores/app";
import { selectIsLoggedIn } from "../stores/slices/auth";

export function ActiveMenu() {
  const isLoggedIn = useAppStore(selectIsLoggedIn);

  return (
    <div className="flex flex-col pb-2">
      {isLoggedIn ? (
        <>
          <a
            className="flex flex-row items-center justify-start gap-3 px-4 py-2 hover:hover:backdrop-brightness-150"
            href={path({ to: "/ucet/pichacky" })}
          >
            <TbAlarm />
            Píchačky
          </a>
          <a
            className="flex flex-row items-center justify-start gap-3 px-4 py-2 hover:hover:backdrop-brightness-150"
            href={path({ to: "/ucet/smeny" })}
          >
            <TbCalendar />
            Směny
          </a>
          <a
            className="flex flex-row items-center justify-start gap-3 px-4 py-2 hover:hover:backdrop-brightness-150"
            href={path({ to: "/ucet" })}
          >
            <TbUser />
            Můj účet
          </a>
          <a
            className="flex flex-row items-center justify-start gap-3 px-4 py-2 hover:hover:backdrop-brightness-150"
            href={path({ to: "/ucet/odhlaseni" })}
          >
            <TbLogout />
            Odhlášení
          </a>
        </>
      ) : (
        <a
          className="flex flex-row items-center justify-start gap-3 px-4 py-2 hover:hover:backdrop-brightness-150"
          href={path({ to: "/prihlaseni" })}
        >
          <TbLogin />
          Přihlášení
        </a>
      )}
    </div>
  );
}
