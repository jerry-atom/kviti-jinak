import { Button } from "@components/form/Button";
import classNames from "classnames";
import type { ReactElement } from "react";
import {
  TbAlarm,
  TbCalendar,
  TbDoorExit,
  TbKey,
  TbReport,
} from "react-icons/tb";
import { useRequireAuth } from "../../../hooks/useRequireAuth";
import api from "../../../libs/api";
import { path } from "../../../libs/routes";
import { useAppStore } from "../../../stores/app";
import { selectIsLoggedIn } from "../../../stores/slices/auth";
import { selectHasUserRole } from "../../../stores/slices/profile";

type ListItemProps = {
  active?: boolean;
  href: string;
  icon: ReactElement;
  label: string;
};
function ListItem({ active, href, icon, label }: ListItemProps) {
  return (
    <a
      className={classNames(
        "flex flex-row justify-start items-center gap-3 border-2 border-primary rounded-md text-primary hover:bg-primary-light p-3",
        {
          "text-primary": active,
        },
      )}
      href={href}
    >
      {icon}
      <span className="text-h4">{label}</span>
    </a>
  );
}

export function Dashboard() {
  useRequireAuth();
  const isLoggedIn = useAppStore(selectIsLoggedIn);
  const isAdmin = useAppStore((state) => selectHasUserRole(state, "admin"));
  const isEmployee = useAppStore((state) =>
    selectHasUserRole(state, "employee"),
  );

  return (
    <>
      <div className="grid grid-cols-2 md:grid-cols-3 gap-3 my-6">
        {isEmployee && (
          <>
            <ListItem
              icon={<TbAlarm size={28} />}
              href={path({ to: "/ucet/pichacky" })}
              label="Píchačky"
            />
            <ListItem
              icon={<TbCalendar size={28} />}
              href={path({ to: "/ucet/smeny" })}
              label="Směny"
            />
            <ListItem
              icon={<TbReport size={28} />}
              href={path({ to: "/ucet/dochazka" })}
              label="Docházka"
            />
          </>
        )}
        {isLoggedIn && (
          <>
            <ListItem
              icon={<TbKey size={28} />}
              href={path({ to: "/ucet/zmena-hesla" })}
              label="Změna hesla"
            />
            <ListItem
              icon={<TbDoorExit size={28} />}
              href={path({ to: "/ucet/odhlaseni" })}
              label="Odhlášení"
            />
          </>
        )}
      </div>
      {isAdmin && (
        <div className="grid grid-cols-2 md:grid-cols-3 gap-3 my-6">
          <Button
            color="danger-outline"
            onClick={() => {
              api.get("/system/web-build/run?force=1").then(
                (response) => {
                  console.log(response);
                },
                (error) => {
                  console.error(error);
                },
              );
            }}
          >
            Sestavit
          </Button>
        </div>
      )}
    </>
  );
}
