import { Message } from "@components/Message";
import { PasswordInput } from "@components/form/PasswordInput";
import { SubmitButton } from "@components/form/SubmitButton";
import { TextInput } from "@components/form/TextInput";
import { useForm, zodResolver } from "@mantine/form";
import { useState } from "react";
import { z } from "zod";
import type { LoginParams } from "../../../services/auth";
import { useAppStore } from "../../../stores/app";

const formSchema = z.object({
  email: z
    .string()
    .min(1, "Zadejte přihlašovací e-mail")
    .email("Přihlašovací e-mail nevypadá správně"),
  password: z.string().min(1, "Zadejte přihlašovací heslo"),
});

enum State {
  NONE = 0,
  LOADING = 1,
  INVALID_DATA = 2,
  SUCCESS = 3,
  UNKNOWN_ERROR = 4,
  WRONG_CREDENTIALS = 5,
}

type Props = {
  onSuccess: () => void;
};

export function SignInForm(props: Props) {
  const login = useAppStore((state) => state.login);
  const logout = useAppStore((state) => state.logout);
  const form = useForm({
    initialValues: {
      email: "",
      password: "",
    },
    validate: zodResolver(formSchema),
  });
  const [state, setState] = useState<State>(State.NONE);
  const onSubmit = form.onSubmit(async (params: LoginParams): Promise<void> => {
    try {
      setState(State.LOADING);

      const result = await login(params);
      if (result.ok) {
        setState(State.SUCCESS);
        props.onSuccess();
      } else {
        await logout();
        switch (result.code) {
          case "password_not_found":
          case "wrong_credentials": {
            setState(State.WRONG_CREDENTIALS);
            break;
          }
          default: {
            setState(State.UNKNOWN_ERROR);
            break;
          }
        }
      }
    } catch (error) {
      console.error(error);
      setState(State.UNKNOWN_ERROR);
    }
  });
  const showFeedback = Object.values(form.errors).length > 0;

  return (
    <form className="grid grid-cols-1 gap-2" onSubmit={onSubmit} noValidate>
      {state === State.INVALID_DATA && (
        <Message color="danger">Přihlášení selhalo</Message>
      )}
      {state === State.UNKNOWN_ERROR && (
        <Message color="danger">Přihlášení selhalo</Message>
      )}
      {state === State.WRONG_CREDENTIALS && (
        <Message color="danger">Špatné přihlašovací údaje.</Message>
      )}
      <TextInput
        autoComplete="email"
        disabled={state === State.LOADING}
        id="email"
        label="E-mail"
        placeholder="pepa.novak@priklad.ie"
        required
        showFeedback={showFeedback}
        type="email"
        {...form.getInputProps("email")}
      />
      <PasswordInput
        autoComplete="current-password"
        disabled={state === State.LOADING}
        id="password"
        label="Heslo"
        required
        showFeedback={showFeedback}
        {...form.getInputProps("password")}
      />
      <SubmitButton disabled={state === State.LOADING}>Přihlásit</SubmitButton>
    </form>
  );
}
