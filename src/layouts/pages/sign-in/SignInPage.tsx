import { FacebookAuthButton } from "@components/FacebookAuthButton";
import { GoogleAuthButton } from "@components/GoogleAuthButton";
import { useMemo } from "react";
import { useNavigate } from "../../../hooks/useNavigate";
import { SignInForm } from "./SignInForm";

export function SignInPage() {
  const navigate = useNavigate();
  const redirect = useMemo(() => {
    const url = new URL(window.location.href);
    let redirect = url.searchParams.get("redirect");
    if (typeof redirect !== "string") {
      redirect = "/";
    }
    return redirect;
  }, []);

  return (
    <div className="flex flex-col gap-3">
      <GoogleAuthButton label="Pokračovat s Google" redirect={redirect} />
      <FacebookAuthButton label="Pokračovat s Facebook" redirect={redirect} />

      <div className="my-3 text-center">NEBO</div>

      <SignInForm
        onSuccess={() => {
          navigate({
            // biome-ignore lint/suspicious/noExplicitAny: it is safe
            to: redirect as any,
          });
        }}
      />
    </div>
  );
}
