import { Message } from "@components/Message";
import { PasswordInput } from "@components/form/PasswordInput";
import { SubmitButton } from "@components/form/SubmitButton";
import { useForm, zodResolver } from "@mantine/form";
import { useState } from "react";
import { z } from "zod";
import { resetPassword } from "../../../services/passwords";
import { TokenExpired } from "./TokenExpired";
import { TokenInvalid } from "./TokenInvalid";
import { TokenResolved } from "./TokenResolved";

const schema = z.object({
  token: z.string().uuid(),
  password: z.string().min(10, "Heslo musí být aspoň 10 znaků dlouhé."),
});

enum State {
  FORM = 0,
  LOADING = 1,
  MALFORMED_TOKEN = 2,
  MISSING_TOKEN = 3,
  SUCCESS = 4,
  TOKEN_EXPIRED = 5,
  TOKEN_INVALID = 6,
  TOKEN_RESOLVED = 7,
  UNKNOWN_ERROR = 8,
}

type PasswordResetFormInnerProps = {
  state: State;
  setState: (state: State) => void;
  token: string | null;
};

function PasswordResetFormInner({
  state,
  setState,
  token,
}: PasswordResetFormInnerProps) {
  const form = useForm({
    initialValues: {
      token: token ?? "",
      password: "",
    },
    validate: zodResolver(schema),
  });
  const onSubmit = form.onSubmit(async (params): Promise<void> => {
    try {
      setState(State.LOADING);
      const result = await resetPassword(params);
      if (result.ok) {
        form.reset();
        setState(State.SUCCESS);
      } else if (result.code === "token_expired") {
        setState(State.TOKEN_EXPIRED);
      } else if (result.code === "token_invalid") {
        setState(State.TOKEN_INVALID);
      } else if (result.code === "token_resolved") {
        setState(State.TOKEN_RESOLVED);
      } else {
        setState(State.UNKNOWN_ERROR);
      }
    } catch (error) {
      console.error(error);
      setState(State.UNKNOWN_ERROR);
    }
  });
  const showFeedback = Object.values(form.errors).length > 0;

  return (
    <form className="grid grid-cols-1 gap-2" onSubmit={onSubmit} noValidate>
      <Message color="info">Zde si nastavte Vaše nové heslo.</Message>

      <div>
        <PasswordInput
          description={
            <>
              Heslo musí být aspoň 10 znaků dlouhé. Zkuste si ho{" "}
              <a
                className="link-primary"
                href="https://www.avast.com/cs-cz/random-password-generator#pc"
                target="_blank"
                rel="noreferrer"
              >
                vygenerovat
              </a>
              .
            </>
          }
          disabled={state === State.LOADING}
          label="Nové heslo"
          id="password"
          name="password"
          required
          showFeedback={showFeedback}
          {...form.getInputProps("password")}
        />
      </div>

      <SubmitButton disabled={state === State.LOADING}>
        Natavit nové heslo
      </SubmitButton>
    </form>
  );
}

type Props = {
  token: null | string;
};

export function PasswordResetForm({ token }: Props) {
  const [state, setState] = useState<State>(State.FORM);

  switch (state) {
    case State.FORM:
      return (
        <PasswordResetFormInner
          state={state}
          setState={setState}
          token={token}
        />
      );
    case State.MALFORMED_TOKEN:
      return (
        <Message color="danger">
          Token není platný. Ověření e-mailu nelze provést.
        </Message>
      );
    case State.MISSING_TOKEN:
      return (
        <Message color="danger">
          URL adresa neobsahuje token. Ověření e-mailu nelze provést.
        </Message>
      );
    case State.SUCCESS:
      return (
        <Message color="success">
          Změna hesla proběhla v pořádku. Můžete se přihlásit.
        </Message>
      );
    case State.LOADING:
      return <Message color="info">Zpracovávávm.</Message>;
    case State.TOKEN_EXPIRED:
      return <TokenExpired />;
    case State.TOKEN_INVALID:
      return <TokenInvalid />;
    case State.TOKEN_RESOLVED:
      return <TokenResolved />;
    default:
      return <Message color="danger">Neznámá chyba.</Message>;
  }
}
