import { Message } from "@components/Message";
import { path } from "../../../libs/routes";

export function TokenResolved() {
  return (
    <Message color="danger">
      <p>Tento odkaz už byl uplatněn. O obnovení hesla můžete zažádat znovu.</p>
      <hr className="border-danger-dark my-3" />
      <a
        className="block float-start bg-danger-dark text-white rounded-md px-3 py-1"
        href={path({ to: "/zapomenute-heslo" })}
      >
        Obnovení hesla
        <i className="bi bi-arrow-right-circle ms-2" />
      </a>
    </Message>
  );
}
