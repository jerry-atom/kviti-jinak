import { Message } from "@components/Message";
import { path } from "../../../libs/routes";

export function TokenInvalid() {
  return (
    <Message color="danger">
      <p>Tento token není platný. O obnovení hesla můžete zažádat znovu.</p>
      <hr className="border-danger-dark my-3" />
      <a
        className="block float-start bg-danger-dark text-white rounded-md px-3 py-1"
        href={path({ to: "/zapomenute-heslo" })}
      >
        Obnovení hesla
      </a>
    </Message>
  );
}
