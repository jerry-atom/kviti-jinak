import { Message } from "@components/Message";
import { path } from "../../../libs/routes";

export function TokenExpired() {
  return (
    <Message color="danger">
      <p>
        Platnost tohoto odkazu vypršela. O obnovení hesla můžete zažádat znovu.
      </p>
      <hr className="border-danger-dark my-3" />
      <a
        className="block float-start bg-danger-dark text-white rounded-md px-3 py-1"
        href={path({ to: "/zapomenute-heslo" })}
      >
        Vem mě pryč
      </a>
    </Message>
  );
}
