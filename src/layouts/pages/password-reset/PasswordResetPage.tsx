import { Spinner } from "@components/Spinner";
import { useEffect, useState } from "react";
import {
  fetchTokenRequest,
  isTokenValid,
} from "../../../services/token-request";
import { PasswordResetForm } from "./PasswordResetForm";
import { TokenExpired } from "./TokenExpired";
import { TokenFailed } from "./TokenFailed";
import { TokenInvalid } from "./TokenInvalid";
import { TokenResolved } from "./TokenResolved";

type State =
  | { state: "failed" }
  | { state: "loading" }
  | { state: "success"; token: string }
  | { state: "token_expired" }
  | { state: "token_invalid" }
  | { state: "token_resolved" };

function ContentSwitch(props: State) {
  switch (props.state) {
    case "loading":
      return <Spinner className="mx-auto my-6 w-12 h-12 text-primary" />;
    case "success":
      return <PasswordResetForm token={props.token} />;
    case "token_expired":
      return <TokenExpired />;
    case "token_resolved":
      return <TokenResolved />;
    case "token_invalid":
      return <TokenInvalid />;
    default:
      return <TokenFailed />;
  }
}

export function PasswordResetPage() {
  const searchParams = new URLSearchParams(window.location.search);
  const token = searchParams.get("token") ?? "";
  const [state, setState] = useState<State>({ state: "loading" });

  useEffect(() => {
    setState({ state: "loading" });

    if (!isTokenValid(token)) {
      return setState({ state: "token_invalid" });
    }

    fetchTokenRequest(token)
      .then((result) => {
        if (result.ok) {
          setState({ state: "success", token });
        } else if (result.code === "token_expired") {
          setState({ state: "token_expired" });
        } else if (result.code === "token_invalid") {
          setState({ state: "token_invalid" });
        } else if (result.code === "token_resolved") {
          setState({ state: "token_resolved" });
        } else if (result.code === "token_not_found") {
          setState({ state: "token_invalid" });
        } else {
          setState({ state: "failed" });
        }
      })
      .catch(() => {
        setState({ state: "failed" });
      });
  }, [token]);

  return <ContentSwitch {...state} />;
}
