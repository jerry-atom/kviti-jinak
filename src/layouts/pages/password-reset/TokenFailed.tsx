import { Message } from "@components/Message";
import { path } from "../../../libs/routes";

export function TokenFailed() {
  return (
    <Message color="danger">
      <p>Načítání odkazu selhalo.</p>
      <hr className="border-danger-dark my-3" />
      <a
        className="block float-start bg-danger-dark text-white rounded-md px-3 py-1"
        href={path({ to: "/prihlaseni" })}
      >
        Vem mě pryč
      </a>
    </Message>
  );
}
