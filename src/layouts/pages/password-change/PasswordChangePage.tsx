import { useRequireAuth } from "../../../hooks/useRequireAuth";
import { PasswordChangeForm } from "./PasswordChangeForm";

export function PasswordChangePage() {
  useRequireAuth();
  return <PasswordChangeForm />;
}
