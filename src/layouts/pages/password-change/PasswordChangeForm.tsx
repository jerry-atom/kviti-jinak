import { Message } from "@components/Message";
import { Button } from "@components/form/Button";
import { PasswordInput } from "@components/form/PasswordInput";
import { useForm, zodResolver } from "@mantine/form";
import { useState } from "react";
import { z } from "zod";
import {
  type ChangePasswordParams,
  changePassword,
} from "../../../services/passwords";

enum State {
  INVALID_DATA = 0,
  LOADING = 1,
  NONE = 2,
  SUCCESS = 3,
  UNKNOWN_ERROR = 4,
  WRONG_PASSWORD = 5,
}

const formSchema = z.object({
  newPassword: z
    .string()
    .trim()
    .min(10, "Nové heslo musí být aspoň 10 znaků dlouhé")
    .max(100, "To nám chcete zacpat databázi? 😊"),
  oldPassword: z.string().min(1, "Vyplňte současné heslo"),
});

export function PasswordChangeForm() {
  const [state, setState] = useState<State>(State.NONE);
  const form = useForm({
    initialValues: {
      newPassword: "",
      oldPassword: "",
    },
    validate: zodResolver(formSchema),
  });
  const onSubmit = form.onSubmit(
    async (params: ChangePasswordParams): Promise<void> => {
      setState(State.LOADING);
      const result = await changePassword(params);
      if (result.ok) {
        setState(State.SUCCESS);
        form.reset();
      } else {
        switch (result.code) {
          case "password_too_weak":
            setState(State.INVALID_DATA);
            break;
          case "wrong_password":
            setState(State.WRONG_PASSWORD);
            break;
          default:
            setState(State.UNKNOWN_ERROR);
            break;
        }
      }
    },
  );

  if (state === State.SUCCESS) {
    return <Message color="success">Změna hesla proběhla v pořádku.</Message>;
  }

  return (
    <form className="grid grid-cols-1 gap-3" onSubmit={onSubmit} noValidate>
      <Message color="info">Nové heslo musí být aspoň 10 znaků dlouhé.</Message>

      {state === State.INVALID_DATA && (
        <Message color="danger">
          Změna hesla neproběhala. Formulář obsahuje chyby.
        </Message>
      )}
      {state === State.WRONG_PASSWORD && (
        <Message color="danger">
          Změna hesla neproběhala. Staré heslo neodpovídá současnému heslu.
        </Message>
      )}
      {state === State.UNKNOWN_ERROR && (
        <Message color="danger">Neznámá chyba.</Message>
      )}

      <div>
        <PasswordInput
          disabled={state === State.LOADING}
          id="old-password"
          label="Současné heslo"
          required
          {...form.getInputProps("oldPassword")}
        />
      </div>

      <div>
        <PasswordInput
          disabled={state === State.LOADING}
          id="new-password"
          label="Nové heslo"
          required
          {...form.getInputProps("newPassword")}
        />
      </div>

      <Button color="primary" disabled={state === State.LOADING} type="submit">
        Změnit heslo
      </Button>
    </form>
  );
}
