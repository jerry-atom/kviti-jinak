import { Message } from "@components/Message";
import { Spinner } from "@components/Spinner";
import { SelectInput } from "@components/form/SelectInput";
import { useState } from "react";
import { useAttendanceReportsByYear } from "../../../services/workshifts";

const dateFormatter = new Intl.DateTimeFormat("cs-CZ", {
  day: "numeric",
  month: "short",
});

function rounding(value: number): number {
  return Math.ceil(value / 30) / 2;
}

const START_YEAR = 2024;
const YEARS = Array(new Date().getFullYear() - START_YEAR + 1)
  .fill(0)
  .map((_, i) => String(START_YEAR + i))
  .map((value) => ({ label: `Rok ${value}`, value }));

export function AttendanceReportPage() {
  const [year, setYear] = useState<number>(() => new Date().getFullYear());
  const { data: reports, isLoading } = useAttendanceReportsByYear(year);

  return (
    <div>
      <div className="mb-2">
        <SelectInput
          data={YEARS}
          id="year"
          label=""
          onChange={(value) => setYear(Number(value))}
          value={String(year)}
        />
      </div>
      {isLoading ? (
        <Spinner />
      ) : reports == null || reports.length === 0 ? (
        <Message color="info">
          V tomto období nejsou žádné záznamy o docházce.
        </Message>
      ) : (
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 2xl:grid-cols-4 gap-2">
          {reports.map((report) => {
            return (
              <div
                key={report.dateEnd}
                className="border-2 border-primary-medium rounded-md p-4"
              >
                <div className="text-xl font-bold">
                  {"Období "}
                  {dateFormatter.format(new Date(report.dateStart))}
                  &ndash;
                  {dateFormatter.format(new Date(report.dateEnd))}
                </div>
                <p className="pt-2">
                  <b>Odpracováno:</b> {rounding(report.totalWorkTime)} h
                  <br />
                  <b>Schválený přesčas:</b>{" "}
                  {rounding(report.totalOvertimeApproved)} h
                  <br />
                  <b>Neschválený přesčas:</b>{" "}
                  {rounding(
                    report.totalOvertime - report.totalOvertimeApproved,
                  )}{" "}
                  h
                  <br />
                  <b>Čas stránvený v přestávkách:</b>{" "}
                  {rounding(report.totalPauseTime)} h
                  <br />
                  <b>Nárok na dovolenou:</b> {rounding(report.vacationClaim)} h
                  <br />
                  <b>Čerpání dovolené:</b> {rounding(report.vacationUsed)} h
                  <br />
                  <b>Čerpání neplaceného volna:</b>{" "}
                  {rounding(report.unpaidLeave)} h
                  <br />
                  <b>Úvazek:</b> {report.contractedHours} h
                </p>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
