import classNames from "classnames";
import { useCallback, useMemo } from "react";
import { TbCircleArrowLeft, TbCircleArrowRight } from "react-icons/tb";
import type { Workshift } from "../../../services/workshifts";
import { WorkshiftDay } from "./WorkshiftDay";

const dayFormatter = new Intl.DateTimeFormat("cs-CZ", {
  weekday: "short",
  day: "numeric",
});
const monthFormatter = new Intl.DateTimeFormat("cs-CZ", {
  month: "long",
  year: "numeric",
});

type Props = {
  currentMonthIsoString: string;
  isLoading: boolean;
  onChangeMonth: (month: Date) => void;
  workshifts: Workshift[];
};

export function WorkshiftsTable({
  currentMonthIsoString,
  isLoading,
  onChangeMonth,
  workshifts,
}: Props) {
  const todayIsoString = new Date().toISOString().substring(0, 10);
  const currentMonth = useMemo<Date>(
    () =>
      new Date(
        new Date(`${currentMonthIsoString.substring(0, 7)}-01T00:00:00.000Z`),
      ),
    [currentMonthIsoString],
  );
  const daysInMonth = useMemo(() => {
    const maxDay = new Date(
      currentMonth.getFullYear(),
      currentMonth.getMonth() + 1,
      0,
    ).getDate();
    return Array(maxDay)
      .fill(0)
      .map((_, index) => {
        const d = new Date(currentMonth);
        d.setDate(index + 1);
        return d;
      });
  }, [currentMonth]);
  const onPreviousMonth = useCallback(() => {
    const newDate = new Date(currentMonth);
    const month = newDate.getMonth();
    if (month === 0) {
      newDate.setMonth(11);
      newDate.setFullYear(newDate.getFullYear() - 1);
    } else {
      newDate.setMonth(newDate.getMonth() - 1);
    }
    onChangeMonth(newDate);
  }, [currentMonth, onChangeMonth]);
  const onNextMonth = useCallback(() => {
    const newDate = new Date(currentMonth);
    const month = newDate.getMonth();
    if (month === 11) {
      newDate.setMonth(0);
      newDate.setFullYear(newDate.getFullYear() + 1);
    } else {
      newDate.setMonth(newDate.getMonth() + 1);
    }
    onChangeMonth(newDate);
  }, [currentMonth, onChangeMonth]);

  return (
    <div className="flex flex-col border border-t-0 border-secondary">
      <div className="flex flex-row justify-between items-center border-t border-secondary">
        <button
          className="bg-slate-300 p-2 w-3/12 sm:w-2/12 text-center"
          onClick={onPreviousMonth}
          type="button"
        >
          <TbCircleArrowLeft className="inline" size={26} />
        </button>
        <div className="flex-grow-1">{monthFormatter.format(currentMonth)}</div>
        <button
          className="bg-slate-300 p-2 w-3/12 sm:w-2/12 text-center"
          onClick={onNextMonth}
          type="button"
        >
          <TbCircleArrowRight className="inline" size={26} />
        </button>
      </div>
      <table>
        <tbody>
          {daysInMonth.map((day) => {
            const dayIsoString = day.toISOString().substring(0, 10);
            const isToday = todayIsoString === dayIsoString;
            const isSunday = day.getDay() === 0;
            return (
              <tr key={day.getTime()} className="border-t border-secondary">
                <td
                  className={classNames(
                    "p-1 w-[1%] text-end text-nowrap border-r border-secondary",
                    isSunday
                      ? isToday
                        ? "bg-warning-medium"
                        : "bg-secondary-medium"
                      : isToday
                        ? "bg-warning-light"
                        : "bg-secondary-light",
                  )}
                >
                  {dayFormatter.format(day)}
                </td>
                <td className="p-1 space-y-1">
                  {isLoading ? (
                    <div
                      key={dayIsoString}
                      className="bg-secondary-light rounded-md px-3 py-2 h-4"
                    />
                  ) : (
                    <WorkshiftDay
                      day={day}
                      workshifts={workshifts}
                      key={dayIsoString}
                    />
                  )}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
