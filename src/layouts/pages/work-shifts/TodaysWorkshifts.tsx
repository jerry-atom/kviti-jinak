import { Message } from "@components/Message";
import { Spinner } from "@components/Spinner";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import React from "react";
import { formatDateTime } from "../../../libs/i18n";
import type { UserId } from "../../../libs/types";
import {
  type WorkshiftId,
  useTodaysWorkshifts,
  useUser,
  useWorkshiftAssignments,
} from "../../../services/workshifts";

dayjs.extend(duration);

export function TodaysWorkshifts() {
  const { data, error, isLoading } = useTodaysWorkshifts();

  if (isLoading) {
    return <Spinner className="mx-auto my-6 w-12 h-12 text-primary" />;
  }
  if (error) {
    return <Message color="warning">{error}</Message>;
  }
  if (!data || data.length === 0) {
    return <Message color="info">Dnes není naplánovaná žádná směna.</Message>;
  }

  return (
    <div className="grid grid-cols-1 gap-2">
      {data.map((workshift) => (
        <div
          className="border border-secondary rounded-md p-3"
          key={workshift.id}
        >
          <div className="text-2xl font-bold">Směna {workshift.name}</div>
          <p>
            Začíná: <strong>{formatDateTime(new Date(workshift.start))}</strong>
            <br />
            Končí: <strong>{formatDateTime(new Date(workshift.end))}</strong>
            <br />
            {workshift.note && (
              <>
                Poznámka: <strong>{workshift.note}</strong>
                <br />
              </>
            )}
            <AssignedEmployees workshiftId={workshift.id} />
          </p>
        </div>
      ))}
    </div>
  );
}

function AssignedEmployees({ workshiftId }: { workshiftId: WorkshiftId }) {
  const { data, error, isLoading } = useWorkshiftAssignments(workshiftId);
  return (
    <>
      Zaměstnanci:{" "}
      {isLoading
        ? "načítám..."
        : (error ??
          data?.map(({ employee }, i) => (
            <React.Fragment key={employee}>
              {i > 0 && ","}
              <strong key={employee}>
                <EmployeeName userId={employee} />
              </strong>
            </React.Fragment>
          )))}
      <br />
    </>
  );
}

function EmployeeName({ userId }: { userId: UserId }) {
  const { data, error, isLoading } = useUser(userId);
  return (
    <strong>
      {isLoading ? "načítám..." : (error ?? data?.name ?? "(neznalezno)")}
    </strong>
  );
}
