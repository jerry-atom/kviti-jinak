import { useState } from "react";
import { useRestrictRole } from "../../../hooks/useRestrictRole";
import { useWorkshifts } from "../../../services/workshifts";
import { WorkshiftsTable } from "./WorkshiftsTable";

export function WorkshiftsPage() {
  useRestrictRole("employee");
  const [currentMonth, setCurrentMonth] = useState<Date>(
    () =>
      new Date(`${new Date().toISOString().substring(0, 7)}-01T00:00:00.000Z`),
  );
  const { data: workshifts, isLoading } = useWorkshifts(currentMonth);

  return (
    <WorkshiftsTable
      currentMonthIsoString={currentMonth.toISOString()}
      isLoading={isLoading}
      onChangeMonth={(month) => {
        setCurrentMonth(month);
      }}
      workshifts={workshifts ?? []}
    />
  );
}
