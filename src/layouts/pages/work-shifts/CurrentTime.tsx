import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import { useEffect, useRef, useState } from "react";

dayjs.extend(duration);

export function CurrentTime({ time }: { time: number }) {
  const timerRef = useRef<ReturnType<typeof setInterval>>(null);
  const [timestamp, setTimestamp] = useState(Date.now());

  useEffect(() => {
    if (time < 0 && !timerRef.current) {
      timerRef.current = setInterval(() => {
        setTimestamp(Date.now());
      }, 1000);
    }
    return () => {
      if (timerRef.current) {
        clearInterval(timerRef.current);
        timerRef.current = null;
      }
    };
  }, [time]);

  const totalTime = time < 0 ? timestamp + time : time;
  const format = totalTime > 86400000 ? "D[d] H:mm:ss" : "H:mm:ss";
  return dayjs.duration(totalTime).format(format);
}
