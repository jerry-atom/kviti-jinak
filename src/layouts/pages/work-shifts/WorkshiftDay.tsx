import classNames from "classnames";
import { useMemo } from "react";
import { TbLock, TbLockOpen } from "react-icons/tb";
import type { UserId } from "../../../libs/types";
import {
  type Workshift,
  useUser,
  useWorkshiftAssignments,
} from "../../../services/workshifts";
import { useAppStore } from "../../../stores/app";

const timeFormatter = new Intl.DateTimeFormat("cs-CZ", {
  minute: "2-digit",
  hour: "numeric",
  hourCycle: "h24",
});

type Props = {
  day: Date;
  workshifts: Workshift[];
};

export function WorkshiftDay({ day, workshifts }: Props) {
  const dayWorkshift = useMemo(() => {
    const dayIsoString = day.toISOString().substring(0, 10);
    return workshifts.filter((x) => x.start.substring(0, 10) === dayIsoString);
  }, [workshifts, day]);

  return dayWorkshift.map((workshift) => (
    <WorkshiftItem key={workshift.id} workshift={workshift} />
  ));
}

function WorkshiftItem({ workshift }: { workshift: Workshift }) {
  const { data: workshiftAssignments } = useWorkshiftAssignments(workshift.id);
  return (
    <div
      className={classNames(
        workshift.isLocked ? "bg-secondary-medium" : "bg-primary-light",
        "rounded-md px-3 py-2",
      )}
    >
      <div className="flex flex-row justify-between">
        <div className="font-bold">{workshift.name}</div>
        <div className="flex-grow-0 flex flex-col items-start">
          {workshift.isLocked ? (
            <TbLock className="inline" />
          ) : (
            <TbLockOpen className="inline" />
          )}
        </div>
      </div>
      <div className="flex flex-row justify-start text-sm">
        od {timeFormatter.format(new Date(workshift.start))} do{" "}
        {timeFormatter.format(new Date(workshift.end))}
      </div>
      <div>
        {workshiftAssignments?.map(({ id, employee }) => (
          <EmployeeBadge key={id} userId={employee} />
        ))}
      </div>
    </div>
  );
}

function EmployeeBadge({ userId }: { selected?: boolean; userId: UserId }) {
  const { data: user, isLoading } = useUser(userId);
  const myUserId = useAppStore((state) => state.user?.id);

  return (
    <span
      className={classNames(
        "rounded-md px-2 py-1 text-black text-xs",
        myUserId === userId ? "bg-success" : "bg-primary-medium",
        "me-1",
      )}
    >
      {isLoading ? "(načítám)" : (user?.name ?? "(nenalezen)")}
    </span>
  );
}
