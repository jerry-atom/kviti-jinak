import { Message } from "@components/Message";
import { Spinner } from "@components/Spinner";
import { Button } from "@components/form/Button";
import dayjs from "dayjs";
import duration from "dayjs/plugin/duration";
import { useCallback, useState, useTransition } from "react";
import { useRestrictRole } from "../../../hooks/useRestrictRole";
import { formatDateTime } from "../../../libs/i18n";
import {
  type AttendanceRecord,
  type AttendanceRecordType,
  appendAttendaceRecord,
  useLatestAttendanceRecords,
} from "../../../services/workshifts";
import { CurrentTime } from "./CurrentTime";
import { TodaysWorkshifts } from "./TodaysWorkshifts";

dayjs.extend(duration);

function getAttendanceTypeName(type: AttendanceRecordType): string {
  switch (type) {
    case "work_start":
      return "Práce příchod";
    case "work_end":
      return "Práce odchod";
    case "pause_start":
      return "Pauza začátek";
    case "pause_end":
      return "Pauza konec";
  }
}

function getCurrentAttendanceStateName(
  type: AttendanceRecordType | null,
): string {
  if (type === null) {
    return "Nejsi v práci";
  }
  switch (type) {
    case "work_start":
    case "pause_end":
      return "Jsi v práci";
    case "work_end":
      return "Nejsi v práci";
    case "pause_start":
      return "Máš pauzu";
  }
}

type AttendanceState = {
  nextTypes: AttendanceRecordType[];
  previousType: AttendanceRecordType | null;
  pauseLength: number;
  workLength: number;
};

function calculateAttendanceState(
  attendanceRecords: AttendanceRecord[],
): AttendanceState {
  let workLength = 0;
  let pauseLength = 0;
  let previousType: AttendanceRecordType | null = null;

  for (const record of attendanceRecords) {
    const time = new Date(record.time).getTime();
    switch (record.type) {
      case "work_start": {
        if (previousType === null || previousType === "work_end") {
          workLength -= time;
          break;
        }
        throw new Error(
          "Unexpected work_start that should follow work_end or be at the beginig",
        );
      }
      case "pause_start": {
        if (previousType === "work_start" || previousType === "pause_end") {
          pauseLength -= time;
          workLength += time;
          break;
        }
        throw new Error(
          "Unexpected pause_start that should follow work_start or pause_end",
        );
      }
      case "pause_end": {
        if (previousType === "pause_start") {
          pauseLength += time;
          workLength -= time;
          break;
        }
        throw new Error("Unexpected pause_end that should follow pause_start");
      }
      case "work_end": {
        if (previousType === "work_start" || previousType === "pause_end") {
          workLength += time;
          break;
        }
        throw new Error(
          "Unexpected work_end that should follow work_start or pause_end",
        );
      }
    }
    previousType = record.type;
  }

  let nextTypes: AttendanceRecordType[];
  if (previousType == null) {
    nextTypes = ["work_start"];
  } else {
    switch (previousType) {
      case "work_end":
        nextTypes = ["work_start"];
        break;
      case "work_start":
        nextTypes = ["pause_start", "work_end"];
        break;
      case "pause_start":
        nextTypes = ["pause_end"];
        break;
      case "pause_end":
        nextTypes = ["pause_start", "work_end"];
        break;
    }
  }

  return { nextTypes, pauseLength, previousType, workLength };
}

const MESSAGE_CONFLICTING_ATTENDANCE = "Špatné pořadí záznamů docházky.";
const MESSAGE_UNKNOWN_ERROR = "Neznámá chyba.";

export function PunchClockPage() {
  useRestrictRole("employee");
  const { data: latestAttendanceRecords, isLoading } =
    useLatestAttendanceRecords();

  if (isLoading) {
    return <Spinner className="mx-auto my-6 w-12 h-12 text-primary" />;
  }

  return (
    <div className="space-y-3">
      <TodaysWorkshifts />
      {latestAttendanceRecords && (
        <PunchClockForm
          attendanceRecords={latestAttendanceRecords}
          isLoading={isLoading}
        />
      )}
    </div>
  );
}

function PunchClockForm({
  attendanceRecords,
  isLoading,
}: {
  attendanceRecords: AttendanceRecord[];
  isLoading: boolean;
}) {
  const { nextTypes, pauseLength, previousType, workLength } =
    calculateAttendanceState(attendanceRecords);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isPending, startTransition] = useTransition();

  const onAppendAttendaceRecord = useCallback(
    async (attendaceRecordType: AttendanceRecordType) => {
      startTransition(async () => {
        try {
          startTransition(() => setErrorMessage(null));
          const result = await appendAttendaceRecord(attendaceRecordType);
          if (result.ok) return;
          if (result.error === "conflicting_attendance") {
            startTransition(() =>
              setErrorMessage(MESSAGE_CONFLICTING_ATTENDANCE),
            );
          } else {
            startTransition(() => setErrorMessage(MESSAGE_UNKNOWN_ERROR));
          }
        } catch (error) {
          console.log(error);
          startTransition(() => setErrorMessage(MESSAGE_UNKNOWN_ERROR));
        }
      });
    },
    [],
  );

  return (
    <>
      <div>
        Aktuální stav:{" "}
        <strong>{getCurrentAttendanceStateName(previousType)}</strong>
        <br />
        Čas v práci: <strong>{<CurrentTime time={workLength} />}</strong>
        <br />
        Čas v pauzách: <strong>{<CurrentTime time={pauseLength} />}</strong>
      </div>
      {errorMessage && (
        <Message color="danger" title="Chyba">
          {errorMessage}
        </Message>
      )}
      <div className="grid grid-cols-1 gap-3">
        {nextTypes.includes("work_start") && (
          <Button
            className="py-4"
            color="success"
            isLoading={isLoading || isPending}
            onClick={() => onAppendAttendaceRecord("work_start")}
            type="button"
          >
            Příchod
          </Button>
        )}
        {nextTypes.includes("work_end") && (
          <Button
            className="py-4"
            color="danger"
            isLoading={isLoading || isPending}
            onClick={() => onAppendAttendaceRecord("work_end")}
            type="button"
          >
            Odchod
          </Button>
        )}
        {nextTypes.includes("pause_start") && (
          <Button
            className="py-4"
            color="success-outline"
            isLoading={isLoading || isPending}
            onClick={() => onAppendAttendaceRecord("pause_start")}
            type="button"
          >
            Pauza začátek
          </Button>
        )}
        {nextTypes.includes("pause_end") && (
          <Button
            className="py-4"
            color="danger-outline"
            isLoading={isLoading || isPending}
            onClick={() => onAppendAttendaceRecord("pause_end")}
            type="button"
          >
            Pauza konec
          </Button>
        )}
        <AttendanceTable attendanceRecords={attendanceRecords} />
      </div>
    </>
  );
}

function AttendanceTable({
  attendanceRecords,
}: { attendanceRecords: AttendanceRecord[] }) {
  return (
    <table className="border border-secondary w-full">
      <thead>
        <tr>
          <th className="border border-secondary p-2" scope="col">
            Čas
          </th>
          <th className="border border-secondary p-2" scope="col">
            Událost
          </th>
        </tr>
      </thead>
      <tbody>
        {attendanceRecords.length === 0 && (
          <tr>
            <td className="border border-secondary p-2 text-center" colSpan={2}>
              Zatím nebyla zalogována žádná událost.
            </td>
          </tr>
        )}
        {attendanceRecords.map((attendanceRecord) => (
          <tr key={attendanceRecord.id}>
            <td className="border border-secondary p-2">
              {formatDateTime(new Date(attendanceRecord.time))}
            </td>
            <td className="border border-secondary p-2">
              {getAttendanceTypeName(attendanceRecord.type)}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}
