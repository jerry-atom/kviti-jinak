import { Message } from "@components/Message";
import { SubmitButton } from "@components/form/SubmitButton";
import { TextInput } from "@components/form/TextInput";
import { useForm, zodResolver } from "@mantine/form";
import { useState } from "react";
import { z } from "zod";
import {
  type ForgettenPasswordParams,
  forgettenPassword,
} from "../../../services/passwords";

enum State {
  LOADING = 0,
  NONE = 1,
  SUCCESS = 2,
  UNKNOWN_ERROR = 3,
}

type Data = {
  email: string;
};

const dataSchema = z.object({
  email: z
    .string()
    .min(1, "Zadejte přihlašovací e-mail")
    .email("Přihlašovací e-mail nevypadá správně"),
});

export function ForgettenPasswordForm() {
  const form = useForm<Data>({
    initialValues: {
      email: "",
    },
    validate: zodResolver(dataSchema),
  });
  const [state, setState] = useState<State>(State.NONE);
  const onSubmit = form.onSubmit(
    async (params: ForgettenPasswordParams): Promise<void> => {
      try {
        setState(State.LOADING);
        const result = await forgettenPassword(params);
        if (result.ok) {
          form.reset();
          setState(State.SUCCESS);
        } else {
          setState(State.UNKNOWN_ERROR);
        }
      } catch (error) {
        console.error(error);
        setState(State.UNKNOWN_ERROR);
      }
    },
  );
  const showFeedback = Object.values(form.errors).length > 0;

  return (
    <form className="flex flex-col gap-3" onSubmit={onSubmit} noValidate>
      {state === State.NONE && (
        <Message color="info">
          Pošlete nám Vaši registrační e-mail&apos;ovou adresu. Pokud u nás máte
          účet, obratem Vám do e-mailové schránky pošleme unikátní odkaz na
          stránku s obnovením hesla. A co se týče nakládání s e-mailovou adresou
          z hlediska GDPR, místopřísežně prohlašujeme, že{" "}
          <a
            className="link-primary"
            href="https://eur-lex.europa.eu/legal-content/CS/TXT/?uri=CELEX:32016R0679#d1e1880-1-1"
          >
            ctíme oprávněný zájem
          </a>
          .
        </Message>
      )}
      {state === State.SUCCESS && (
        <Message color="success">
          Vaši žádost jsme přijali. Pokud u nás máte účet, obratem Vám do
          e-mailové schránky pošleme unikátní odkaz na stránku s obnovením
          hesla.
        </Message>
      )}
      {state === State.UNKNOWN_ERROR && (
        <Message color="danger">Neznámá chyba.</Message>
      )}

      <TextInput
        autoComplete="email"
        label="E-mail"
        id="email"
        name="email"
        placeholder="pepa.novak@example.com"
        required
        showFeedback={showFeedback}
        {...form.getInputProps("email")}
      />

      <SubmitButton disabled={state === State.LOADING}>
        Pošlete mi odkaz
      </SubmitButton>
    </form>
  );
}
