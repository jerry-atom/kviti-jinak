import { Button } from "@components/form/Button";
import { useRequireAuth } from "../../../hooks/useRequireAuth";
import { path } from "../../../libs/routes";
import { useAppStore } from "../../../stores/app";

export function SignOutPage() {
  useRequireAuth();
  const logout = useAppStore((state) => state.logout);
  const clear = useAppStore((state) => state.clear);

  return (
    <div className="text-center">
      <p className="font-h2 my-6">Opravdu se chcete odhlásit?</p>
      <Button
        onClick={() => {
          logout().finally(() => {
            clear();
            const url = new URL(window.location.href);
            url.pathname = path({ to: "/" });
            window.location.href = url.toString();
          });
        }}
        color="danger"
      >
        Ano, chci se odhlásit
      </Button>
    </div>
  );
}
