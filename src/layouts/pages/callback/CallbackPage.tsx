import { Message } from "@components/Message";
import { Spinner } from "@components/Spinner";
import { useEffect, useState } from "react";
import { path } from "../../../libs/routes";
import { useAppStore } from "../../../stores/app";

type State = { state: "failed"; message: string } | { state: "loading" };

export default function CallbackPage() {
  const setAccessToken = useAppStore((state) => state.setAccessToken);
  const loadProfile = useAppStore((state) => state.loadProfile);
  const [state, setState] = useState<State>({ state: "loading" });

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const jwtToken = params.get("token");
    const redirectUri = params.get("redirect_uri");

    if (jwtToken) {
      setAccessToken(jwtToken);

      loadProfile()
        .then((result) => {
          if (result.ok) {
            if (redirectUri) {
              window.location.href = redirectUri;
            } else {
              window.location.href = path({ to: "/ucet" });
            }
          } else if (result.code === "not_found") {
            setState({
              state: "failed",
              message: "Data k účtu nebyla nalezena",
            });
          } else if (result.code === "unauthorized") {
            setState({ state: "failed", message: "Přihlášení nebylo úspěšné" });
          } else if (result.code === "unknown") {
            setState({ state: "failed", message: "Token není platný" });
          }
        })
        .catch(() => {
          setState({ state: "failed", message: "Přihlášení nebylo úspěšné" });
        });
    }
  }, [setAccessToken, loadProfile]);

  return (
    <div className="flex flex-col gap-3 items-center">
      {state.state === "loading" && (
        <>
          <Spinner className="mt-12" aria-hidden="true" />
          <output className="text-h2">Načítám</output>
        </>
      )}
      {state.state === "failed" && (
        <Message color="danger" title="Chyba!">
          <p>{state.message}</p>
          <hr />
          <p className="mb-0">
            Zkuste se znovu <a href={path({ to: "/prihlaseni" })}>přihlásit</a>,
            případně kontaktujte naši{" "}
            <a href="mailto:jaroslav.novotny.84@gmail.com">
              technickou podporu
            </a>
            .
          </p>
        </Message>
      )}
    </div>
  );
}
