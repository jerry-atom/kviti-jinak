import { FacebookAuthButton } from "@components/FacebookAuthButton";
import { GoogleAuthButton } from "@components/GoogleAuthButton";
import { RegistrationForm } from "./RegistrationForm";

export function RegistrationPage() {
  return (
    <div className="flex flex-col gap-3 mx-auto">
      <GoogleAuthButton label="Pokračovat s Google" redirect="/" />
      <FacebookAuthButton label="Pokračovat s Facebook" redirect="/" />

      <div className="text-center">NEBO</div>

      <RegistrationForm />
    </div>
  );
}
