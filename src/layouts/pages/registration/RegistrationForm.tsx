import { Message } from "@components/Message";
import { CheckboxInput } from "@components/form/CheckboxInput";
import { PasswordInput } from "@components/form/PasswordInput";
import { SubmitButton } from "@components/form/SubmitButton";
import { TextInput } from "@components/form/TextInput";
import { useForm, zodResolver } from "@mantine/form";
import { useState } from "react";
import { z } from "zod";
import { path } from "../../../libs/routes";
import { type RegisterData, register } from "../../../services/register";

const schema = z.object({
  email: z.string().email("E-mail nemá správný formát."),
  name: z.string().min(1, "Nesmí být prázdné."),
  password: z.string().min(10, "Heslo musí být aspoň 10 znaků dlouhé."),
  phone: z
    .string()
    .regex(
      /^(\+\d{3})\d{9}$/,
      "Telefon nemá správný formát. Správný formát je např. +420123456789.",
    )
    .nullable(),
  termsOfService: z.literal(true, {
    errorMap: () => ({
      message:
        "Pro dokončení Vaší žádosti o registraci potřebujeme souhlas s našimi všeobecnými podmínkami.",
    }),
  }),
});

enum State {
  INIT = 0,
  INVALID_DATA = 1,
  LOADING = 2,
  SUCCESS = 3,
  UNKNOWN_ERROR = 4,
}

export function RegistrationForm() {
  const form = useForm({
    initialValues: {
      email: "",
      name: "",
      password: "",
      phone: "",
      termsOfService: false,
    },
    validate: zodResolver(schema),
  });
  const [state, setState] = useState(State.INIT);
  const onSubmit = form.onSubmit(
    async (params: RegisterData): Promise<void> => {
      try {
        setState(State.LOADING);
        await register(params);
        setState(State.SUCCESS);
        form.reset();
      } catch (error) {
        console.error(error);
        setState(State.UNKNOWN_ERROR);
      }
    },
  );
  const isLoading = state === State.LOADING;
  const showFeedback = Object.values(form.errors).length > 0;

  return (
    <form className="flex flex-col gap-3" onSubmit={onSubmit} noValidate>
      {state === State.INIT && (
        <Message color="info" title="Chyba!">
          Odešlete formulář a my Vám pošleme e-mail s odkazem pro ověření Vaší
          registrace.
        </Message>
      )}
      {state === State.UNKNOWN_ERROR && (
        <Message color="danger">Registrace selhala z neznámého důvodu.</Message>
      )}
      {state === State.SUCCESS && (
        <Message color="success">
          Registrace proběhla v pořádku. Poslali jsme Vám e-mail s odkazem pro
          ověření Vašeho e-mailu.
        </Message>
      )}

      <TextInput
        autoComplete="email"
        disabled={isLoading}
        label="E-mail"
        id="email"
        placeholder="pepa.novak@example.com"
        required
        showFeedback={showFeedback}
        type="email"
        {...form.getInputProps("email")}
      />
      <PasswordInput
        autoComplete="new-password"
        description={
          <>
            Heslo musí být aspoň 10 znaků dlouhé. Zkuste si ho{" "}
            <a
              className="link-primary"
              href="https://www.avast.com/cs-cz/random-password-generator#pc"
              target="_blank"
              rel="noreferrer"
            >
              vygenerovat
            </a>
            .
          </>
        }
        disabled={isLoading}
        label="Heslo"
        id="password"
        name="password"
        placeholder="aspoň 10 znaků"
        required
        showFeedback={showFeedback}
        {...form.getInputProps("password")}
      />
      <TextInput
        autoComplete="name"
        disabled={isLoading}
        id="name"
        label="Celé jméno"
        name="name"
        placeholder="Pepa Novák"
        required
        showFeedback={showFeedback}
        type="text"
        {...form.getInputProps("name")}
      />
      <TextInput
        autoComplete="tel"
        disabled={isLoading}
        id="phone"
        label="Telefonní číslo"
        name="phone"
        placeholder="+420558840007"
        required
        showFeedback={showFeedback}
        type="tel"
        {...form.getInputProps("phone")}
      />
      <CheckboxInput
        disabled={isLoading}
        id="termsOfService"
        label={
          <>
            Souhlasím se zpracováním osobních údajů dle{" "}
            <a
              className="link-primary"
              href={path({ to: "/obchodni-podminky" })}
            >
              obchodních podmínek
            </a>
            .
          </>
        }
        name="termsOfService"
        required
        {...form.getInputProps("termsOfService", { type: "checkbox" })}
      />
      <SubmitButton disabled={isLoading}>Založte mi účet</SubmitButton>
    </form>
  );
}
