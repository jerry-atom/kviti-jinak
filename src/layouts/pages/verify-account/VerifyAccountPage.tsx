import { Message } from "@components/Message";
import { useEffect, useState } from "react";
import { path } from "../../../libs/routes";
import { isTokenValid } from "../../../services/token-request";
import { verifyAccount } from "../../../services/verify-account";

type State =
  | { state: "failed" }
  | { state: "loading" }
  | { state: "success" }
  | { state: "token_expired" }
  | { state: "token_invalid" }
  | { state: "token_resolved" };

function ContentSwitch(props: State) {
  switch (props.state) {
    case "success":
      return (
        <Message color="success" title="Hotovo!">
          <p>
            Ověření registrace proběhlo v pořádku. Nyní se můžete přihlásit.
          </p>
          <hr />
          <a className="btn btn-success" href={path({ to: "/prihlaseni" })}>
            Přihášení
            <i className="bi bi-arrow-right-circle ms-2" />
          </a>
        </Message>
      );
    case "token_expired":
      return (
        <Message color="danger" title="Chyba!">
          <p>
            Ověření e-mailu nelze provést, protože platnost tokenu vypršela.
            Registrujte se znovu.
          </p>
          <hr />
          <a className="btn btn-danger" href={path({ to: "/registrace" })}>
            Registrace
            <i className="bi bi-arrow-right-circle ms-2" />
          </a>
        </Message>
      );
    case "token_resolved":
      return (
        <Message color="danger" title="Chyba!">
          <p>Tento token už byl uplatněn. Můžete se přihlásit.</p>
          <hr />
          <a className="btn btn-danger" href={path({ to: "/prihlaseni" })}>
            Přihášení
            <i className="bi bi-arrow-right-circle ms-2" />
          </a>
        </Message>
      );
    default:
      return (
        <Message color="danger" title="Chyba!">
          <p>Načítání tokenu selhalo.</p>
          <hr />
          <a className="btn btn-danger" href={path({ to: "/prihlaseni" })}>
            Vem mě pryč
            <i className="bi bi-arrow-right-circle ms-2" />
          </a>
        </Message>
      );
  }
}

export function VerifyAccountPage() {
  const searchParams = new URLSearchParams(window.location.search);
  const token = searchParams.get("token") ?? "";
  const [state, setState] = useState<State>({ state: "loading" });

  useEffect(() => {
    setState({ state: "loading" });

    if (!isTokenValid(token)) {
      return setState({ state: "token_invalid" });
    }

    verifyAccount(token)
      .then((result) => {
        if (result.ok) {
          setState({ state: "success" });
        } else if (result.code === "token_expired") {
          setState({ state: "token_expired" });
        } else if (result.code === "token_resolved") {
          setState({ state: "token_resolved" });
        } else {
          setState({ state: "token_invalid" });
        }
      })
      .catch(() => {
        setState({ state: "failed" });
      });
  }, [token]);

  return (
    <>
      <p className="text-center">Token: {token}</p>
      <ContentSwitch {...state} />
    </>
  );
}
