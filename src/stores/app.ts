import { create } from "zustand";
import { devtools, persist, subscribeWithSelector } from "zustand/middleware";
import {
  type Slice as AuthSlice,
  type State as AuthState,
  createAuthSlice,
  selectIsLoggedIn,
} from "./slices/auth";
import {
  type Slice as ProfileSlice,
  type State as ProfileState,
  createProfileSlice,
} from "./slices/profile";

type AppOwnActions = {
  clear: () => void;
};
type AppOwnSlice = AppOwnActions;
export type AppState = AuthState & ProfileState;
type AppSlice = AppOwnSlice & AuthSlice & ProfileSlice;

export const useAppStore = create<AppSlice>()(
  devtools(
    subscribeWithSelector(
      persist(
        (...a) => ({
          ...createAuthSlice(...a),
          ...createProfileSlice(...a),
          clear: () => {
            const { setAccessToken, clearProfile } = a[1]();
            clearProfile();
            setAccessToken("");
          },
        }),
        {
          name: "app",
        },
      ),
    ),
  ),
);

const isLoggedIn = selectIsLoggedIn(useAppStore.getState());
if (isLoggedIn) {
  void useAppStore.getState().loadProfile();
} else {
  void useAppStore.getState().clearProfile();
  void useAppStore.getState().setAccessToken();
}

useAppStore.subscribe(
  (state) => selectIsLoggedIn(state),
  (isLoggedIn) => {
    if (isLoggedIn) {
      void useAppStore.getState().loadProfile();
    } else {
      void useAppStore.getState().clearProfile();
      void useAppStore.getState().setAccessToken();
    }
  },
);
