import type { StateCreator } from "zustand";
import type { Role, User } from "../../libs/types";
import { fetchProfile } from "../../services/profile";

export type State = {
  user: User | null;
};
export type Actions = {
  clearProfile: () => void;
  loadProfile: typeof fetchProfile;
  updateUser: (user: Partial<User>) => void;
};
export type Slice = Actions & State;

export const createProfileSlice: StateCreator<Slice, [], [], Slice> = (
  set,
  get,
) => ({
  user: null,
  clearProfile: () => {
    set({
      user: null,
    });
  },
  loadProfile: async () => {
    const result = await fetchProfile();
    if (result.ok) {
      set((state) => ({
        ...state,
        user: result.user,
      }));
    }
    return result;
  },
  updateUser: (user) => {
    const oldUser = selectUser(get());
    if (oldUser) {
      set((state) => ({ ...state, user: { ...oldUser, ...user } }));
    }
  },
});

export function selectUser(state: State): User | null {
  return state.user;
}

export function selectHasUserRole(state: State, role: Role): boolean {
  const user = selectUser(state);
  return user?.role.includes(role) ?? false;
}
