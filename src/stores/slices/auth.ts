import type { StateCreator } from "zustand";
import {
  login as loginService,
  logout as logoutService,
} from "../../services/auth";

export type State = {
  accessToken?: string;
};
type Actions = {
  login: typeof loginService;
  logout: typeof logoutService;
  setAccessToken: (accessToken?: string) => void;
};
export type Slice = State & Actions;

export const createAuthSlice: StateCreator<Slice, [], [], Slice> = (set) => ({
  login: async (params) => {
    const result = await loginService(params);
    set(() => ({ accessToken: result.ok ? result.token : void 0 }));
    return result;
  },
  logout: async () => {
    const result = await logoutService();
    if (result.ok) {
      set(() => ({ accessToken: void 0 }));
    }
    return result;
  },
  setAccessToken: (accessToken) => {
    set(() => ({ accessToken }));
  },
});

export function selectAccessToken(state: State): string | null {
  return state.accessToken ?? null;
}
export function selectIsLoggedIn(state: State): boolean {
  const accessToken = selectAccessToken(state);
  return accessToken !== "" && accessToken != null;
}
