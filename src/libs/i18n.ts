const dateFormatter = new Intl.DateTimeFormat("cs-CZ", {
  month: "numeric",
  year: "numeric",
  day: "numeric",
  timeZone: "Europe/Prague",
});
const dateTimeFormatter = new Intl.DateTimeFormat("cs-CZ", {
  month: "numeric",
  year: "numeric",
  day: "numeric",
  hour: "2-digit",
  minute: "2-digit",
  second: "2-digit",
  timeZone: "Europe/Prague",
});
const timeFormatter = new Intl.DateTimeFormat("cs-CZ", {
  hour: "2-digit",
  minute: "2-digit",
  timeZone: "Europe/Prague",
});
const currencyFormatter = new Intl.NumberFormat("cs-CZ", {
  style: "currency",
  currency: "CZK",
});
const percentFormatter = new Intl.NumberFormat("cs-CZ", { style: "percent" });

export const formatDate = dateFormatter.format;
export const formatDateTime = dateTimeFormatter.format;
export const formatTime = timeFormatter.format;
export const formatCurrency = currencyFormatter.format;
export const formatPercent = percentFormatter.format;
