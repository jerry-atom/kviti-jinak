import type { PictureId } from "./types";

export type Route =
  | { to: "/" }
  | { to: "/clanky" }
  | { to: "/clanky/{slug}"; slug: string }
  | { to: "/kontakt" }
  | { to: "/o-nas" }
  | { to: "/obchodni-podminky" }
  | { to: "/overeni-emailu"; params: { token: string } }
  | { to: "/prihlaseni" }
  | { to: "/registrace" }
  | { to: "/resetovani-hesla"; params: { token: string } }
  | { to: "/ucet" }
  | { to: "/ucet/dochazka" }
  | { to: "/ucet/odhlaseni" }
  | { to: "/ucet/pichacky" }
  | { to: "/ucet/smeny" }
  | { to: "/ucet/zmena-hesla" }
  | { to: "/zapomenute-heslo" }
  | { to: "/zasady-ochrany-osobnich-udaju" };

type RouteOptions = {
  absolute?: boolean;
};

const siteUrl = import.meta.env.PUBLIC_SITE_URL;

export function path(route: Route, options: RouteOptions = {}): string {
  const url = new URL(siteUrl);
  if (route.to === "/clanky/{slug}") {
    url.pathname = route.to.replace("{slug}", route.slug);
  } else {
    url.pathname = route.to;
  }
  if (route.to === "/resetovani-hesla" || route.to === "/overeni-emailu") {
    for (const [key, value] of Object.entries(route.params)) {
      url.searchParams.set(key, String(value));
    }
  }
  if (options.absolute) {
    return url.toString();
  }
  return url.pathname + url.search + url.hash;
}

export type PictureOptions = {
  height?: number;
  width?: number;
};

export function pictureAbsolute(
  id: PictureId,
  type: string,
  options: PictureOptions = {},
): string {
  const url = new URL(import.meta.env.PUBLIC_API_URL);
  url.pathname = `/pictures/${id}.${type}`;
  if (options.width) {
    url.searchParams.set("width", String(options.width));
  }
  if (options.height) {
    url.searchParams.set("height", String(options.height));
  }
  return url.toString();
}

export function picture(id: PictureId, type: string): string {
  return `${import.meta.env.PUBLIC_API_URL}/pictures/${id}.${type}`;
}

export function noImageAvailablePicture(): string {
  return `${import.meta.env.PUBLIC_SITE_URL}/public/no-image-placeholder.svg`;
}
