import { z } from "zod";

export type Predicate<T> = (x: T) => boolean;

export type NonEmptyArray<T = unknown> = [T, ...T[]];

export type Role = "admin" | "employee" | "user";

export const UserId = z.string().uuid().brand("UserId");
export type UserId = z.infer<typeof UserId>;

export type User = {
  id: UserId;
  name: string;
  email: string;
  phone: string | null;
  role: Role[];
};

export const PictureIdSchema = z.string().uuid().brand("PictureId");
export type PictureId = z.infer<typeof PictureIdSchema>;

export type PictureType = "jpg" | "png" | "webp" | "avif";

export type PictureSourceType = "cloudinary" | "gallery" | "web";

type BasePicture<T extends PictureSourceType> = {
  id: PictureId;
  name: string;
  type: PictureType;
  source: T;
};

type CloudinaryPicture = BasePicture<"cloudinary"> & {
  props: {
    path: string;
  };
};

type GalleryPicture = BasePicture<"gallery"> & {
  props: object;
};

type WebPicture = BasePicture<"web"> & {
  props: {
    url: string;
  };
};

export type Picture = CloudinaryPicture | GalleryPicture | WebPicture;
