import type { ImageTransformationOptions } from "cloudinary";
import { type PictureOptions, pictureAbsolute } from "./routes";
import type { Picture } from "./types";

const cloudinaryPath = `https://res.cloudinary.com/${import.meta.env.PUBLIC_CLOUDINARY_CLOUD_NAME}/image/upload/`;

type Transformation = ImageTransformationOptions;

export function getCloudinaryUrl(
  imageId: string,
  transformations: Transformation[],
) {
  const path: string[] = [];
  for (const transformation of transformations) {
    const entry: string[] = [];
    if (transformation.background) {
      entry.push(`b_${transformation.background}`);
    }
    if (transformation.crop) {
      entry.push(`c_${transformation.crop}`);
    }
    if (transformation.format) {
      entry.push(`f_${transformation.format}`);
    }
    if (transformation.gravity) {
      entry.push(`g_${transformation.gravity}`);
    }
    if (transformation.height) {
      entry.push(`h_${transformation.height}`);
    }
    if (transformation.quality) {
      entry.push(`q_${transformation.quality}`);
    }
    if (transformation.width) {
      entry.push(`w_${transformation.width}`);
    }
    if (entry.length > 0) {
      path.push(`${entry.join()}/`);
    }
  }
  return cloudinaryPath + path.join("") + imageId;
}

function mergeOptions(
  listOfTransformations: Record<string, unknown>[],
): PictureOptions {
  const options: Record<string, unknown> = {};
  for (const transformations of listOfTransformations) {
    for (const [key, value] of Object.entries(transformations)) {
      options[key] = value;
    }
  }
  return options;
}

export function getPictureUrl(
  picture: Picture,
  transformations?: Transformation | Transformation[],
) {
  const listOfTransformations = Array.isArray(transformations)
    ? transformations
    : transformations == null
      ? []
      : [transformations];

  switch (picture.source) {
    case "cloudinary":
      return getCloudinaryUrl(picture.props.path, listOfTransformations);
    case "gallery":
      return pictureAbsolute(
        picture.id,
        picture.type,
        mergeOptions(listOfTransformations),
      );
    case "web":
      return picture.props.url;
  }
}
