import axios from "axios";
import { useAppStore } from "../stores/app";
import { selectAccessToken } from "../stores/slices/auth";

export const api = axios.create({
  adapter: "fetch",
  baseURL: import.meta.env.PUBLIC_API_URL,
  fetchOptions: {
    credentials: "include",
    mode: "cors",
  },
});

api.interceptors.request.use(
  (config) => {
    config.headers["Accept-Language"] = "cs-CZ";
    config.headers.Accept = "application/json; charset=utf-8";
    config.headers["Content-Type"] = "application/json; charset=utf-8";
    const accessToken = selectAccessToken(useAppStore.getState());
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => Promise.reject(error),
);

api.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;

    if (
      error.response &&
      error.response.status === 401 &&
      !originalRequest._retry
    ) {
      originalRequest._retry = true;

      try {
        const response = await axios.post(
          `${import.meta.env.PUBLIC_API_URL}/account/refresh`,
          {},
          {
            withCredentials: true,
          },
        );
        const { token } = response.data;
        useAppStore.getState().setAccessToken(token);

        originalRequest.headers.Authorization = `Bearer ${token}`;
        return axios(originalRequest);
      } catch (error) {
        console.error(error);
        useAppStore.getState().logout();
        // Handle refresh token error or redirect to login
      }
    }

    return Promise.reject(error);
  },
);

export const fetcher = <T>(url: string) =>
  api.get<T>(url).then((res) => res.data);
export default api;
