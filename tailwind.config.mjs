import colors from "tailwindcss/colors";
import TypographyPlugin from "@tailwindcss/typography";
import AnimatePlugin from "tailwindcss-animate";

let font_base = 16;
let font_scale = 1.2;
let h6 = font_scale;
let h5 = h6 * font_scale;
let h4 = h5 * font_scale;
let h3 = h4 * font_scale;
let h2 = h3 * font_scale;
let h1 = h2 * font_scale;
let fontPrimaryType = "Poppins:wght@400;500;600;700";
let fontSecondaryType = "Merriweather:wght@700;900";

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,tsx}"],
  darkMode: "class",
  theme: {
    fontFamily: {
      primary: [fontPrimaryType, "sans-serif"],
      secondary: [fontSecondaryType, "sans-serif"],
    },
    container: {
      center: "true",
      padding: "1rem",
    },
    extend: {
      colors: {
        ebony: {
          50: "oklch(95.05% 0.004 121.56 / <alpha-value>)",
          100: "oklch(90.45% 0.007 115.72 / <alpha-value>)",
          200: "oklch(80.94% 0.015 115.1 / <alpha-value>)",
          300: "oklch(70.87% 0.022 116.14 / <alpha-value>)",
          400: "oklch(60.44% 0.028 116.91 / <alpha-value>)",
          500: "oklch(48.21% 0.022 117.06 / <alpha-value>)",
          600: "oklch(41.48% 0.018 115.55 / <alpha-value>)",
          700: "oklch(34.35% 0.015 117.4 / <alpha-value>)",
          800: "oklch(26.85% 0.011 122.17 / <alpha-value>)",
          900: "oklch(18.55% 0.006 106.97 / <alpha-value>)",
          950: "oklch(13.39% 0.003 106.74 / <alpha-value>)",
        },
        ring: colors.indigo[500],
        danger: {
          DEFAULT: colors.rose[500],
          dark: colors.rose[950],
          light: colors.rose[100],
          medium: colors.rose[300],
        },
        info: {
          DEFAULT: colors.indigo[500],
          dark: colors.indigo[950],
          light: colors.indigo[100],
          medium: colors.indigo[300],
        },
        warning: {
          DEFAULT: colors.amber[500],
          dark: colors.amber[950],
          light: colors.amber[100],
          medium: colors.amber[300],
        },
        success: {
          DEFAULT: colors.emerald[500],
          dark: colors.emerald[950],
          light: colors.emerald[100],
          medium: colors.emerald[300],
        },
        text: {
          DEFAULT: colors.black,
          dark: colors.slate[950],
          light: colors.slate[700],
        },
        primary: {
          DEFAULT: "oklch(48.21% 0.022 117.06 / <alpha-value>)",
          dark: "oklch(13.39% 0.003 106.74 / <alpha-value>)",
          light: "oklch(90.45% 0.007 115.72 / <alpha-value>)",
          medium: "oklch(70.87% 0.022 116.14 / <alpha-value>)",
        },
        secondary: {
          DEFAULT: colors.zinc[500],
          dark: colors.zinc[950],
          light: colors.zinc[100],
          medium: colors.zinc[300],
        },
        body: colors.white,
      },
      fontSize: {
        base: font_base + "px",
        "base-sm": font_base * 0.8 + "px",
        h1: h1 + "rem",
        "h1-sm": h1 * 0.9 + "rem",
        h2: h2 + "rem",
        "h2-sm": h2 * 0.9 + "rem",
        h3: h3 + "rem",
        "h3-sm": h3 * 0.9 + "rem",
        h4: h4 + "rem",
        h5: h5 + "rem",
        h6: h6 + "rem",
      },
      fontFamily: {
        primary: ["var(--font-primary)", fontPrimaryType, "sans-serif"],
        secondary: ["var(--font-secondary)", fontSecondaryType, "sans-serif"],
      },
    },
  },
  plugins: [TypographyPlugin, AnimatePlugin],
};
